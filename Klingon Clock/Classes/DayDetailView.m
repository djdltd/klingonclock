//
//  DayDetailView.m
//  Klingon Clock
//
//  Created by Danny Draper on 31/12/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import "DayDetailView.h"


@implementation DayDetailView

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super initWithCoder:decoder])
    {
		_landscape = false;
		_numericweekday = 0;
		_klingonmode = false;
		self.backgroundColor = [UIColor clearColor];
		
		[_days = [[DaysTemplate alloc] init] retain];
		[_days Initialise];
		[_days setLocation:15.0f:5.0f];
		[_days setNumericWeekday:0];
		
		[_klingonday = [[ClockNumbers alloc] init] retain];
		[_klingonday Initialise:7.0f:4.0f:39.0f:46.0f:false:0.0f:@"DateNumbersKlingon.png"];
		[_klingonday setPosition:50.0f: 5.0f];
		
		
		[self setNeedsDisplay];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
    }
    return self;
}

- (void)setLandscapemode:(bool)landscape
{
	_landscape = landscape;
	
	[self repositionDay];
	[self setNeedsDisplay];
}

- (void)setKlingonmode:(bool)klingonmode
{
	_klingonmode = klingonmode;
	
	[self setNeedsDisplay];
}

- (void)repositionDay
{
	if (_landscape == true) {
		if (_numericweekday == 1) {[_days setLocation:145.0f:5.0f];} // Sunday
		if (_numericweekday == 2) {[_days setLocation:145.0f:5.0f];} // Monday
		if (_numericweekday == 3) {[_days setLocation:140.0f:5.0f];} // Tuesday 
		if (_numericweekday == 4) {[_days setLocation:100.0f:5.0f];} // Wednesday
		if (_numericweekday == 5) {[_days setLocation:110.0f:5.0f];} // Thursday
		if (_numericweekday == 6) {[_days setLocation:155.0f:5.0f];} // Friday
		if (_numericweekday == 7) {[_days setLocation:110.0f:5.0f];} // Saturday
		
		[_klingonday setPosition:240.0f:5.0f];
		
	} else {
		if (_numericweekday == 1) {[_days setLocation:80.0f:5.0f];} // Sunday
		if (_numericweekday == 2) {[_days setLocation:80.0f:5.0f];} // Monday
		if (_numericweekday == 3) {[_days setLocation:70.0f:5.0f];} // Tuesday 
		if (_numericweekday == 4) {[_days setLocation:35.0f:5.0f];} // Wednesday
		if (_numericweekday == 5) {[_days setLocation:50.0f:5.0f];} // Thursday
		if (_numericweekday == 6) {[_days setLocation:90.0f:5.0f];} // Friday
		if (_numericweekday == 7) {[_days setLocation:50.0f:5.0f];} // Saturday
		
		[_klingonday setPosition:180.0f:5.0f];
	}
}

- (void) setNumericWeekday:(int)weekday
{
	NSNumber *numweekday;
	
	_numericweekday = weekday;
	[_days setNumericWeekday:weekday];
	
	numweekday = [[NSNumber alloc] initWithInt:weekday];
	[_klingonday setClockstring:[numweekday stringValue]];
	
	[self repositionDay];
	[self setNeedsDisplay];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
	
	if (_klingonmode == true) {
		[_klingonday PaintString];
	} else {
		[_days PaintString];
	}
	
}


- (void)dealloc {
    [super dealloc];
}


@end
