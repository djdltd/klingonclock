//
//  DayDetailView.h
//  Klingon Clock
//
//  Created by Danny Draper on 31/12/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DaysTemplate.h"
#import "ClockNumbers.h"

@interface DayDetailView : UIView {
	
	DaysTemplate *_days;
	ClockNumbers *_klingonday;
	
	bool _landscape;
	bool _klingonmode;
	int _numericweekday;
}

- (void)setLandscapemode:(bool)landscape;
- (void)setKlingonmode:(bool)klingonmode;
- (void)setNumericWeekday:(int)weekday;
- (void)repositionDay;

@end
