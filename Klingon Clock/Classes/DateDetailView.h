//
//  DateDetailView.h
//  Klingon Clock
//
//  Created by Danny Draper on 31/12/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MonthsTemplate.h"
#import "ClockNumbers.h"

@interface DateDetailView : UIView {

	bool _landscape;
	bool _klingonmode;
	int _numericmonth;
	
	MonthsTemplate *_months;
	ClockNumbers *_date;
	ClockNumbers *_year;
	
	ClockNumbers *_klingonmonth;
}

- (void)setLandscapemode:(bool)landscape;
- (void)setKlingonmode:(bool)klingonmode;
- (void)setNumericMonth:(int)numericmonth;
- (void)repositionMonth;
- (void)setDateString:(NSString *) datestring;
- (void)setYearString:(NSString *) yearstring;

@end
