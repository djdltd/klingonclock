//
//  ClockView.h
//  Klingon Clock
//
//  Created by Danny Draper on 20/12/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVAudioPlayer.h>
#import "ClockDetailView.h"
#import "DateDetailView.h"
#import "DayDetailView.h"
#import "AlarmDetailView.h"

typedef enum {
	AdjHourUp,
	AdjHourDown,
	AdjMinuteUp,
	AdjMinuteDown,
	AdjSecondUp,
	AdjSecondDown,
	AdjNone
} AlarmAdjustMode;

@interface ClockView : UIView<AVAudioPlayerDelegate> {
	IBOutlet UIImageView *imgview_background;
	
	
	// Landscape Speak Time touch buttons
	IBOutlet UIButton *btn_speakbtn1;
	IBOutlet UIButton *btn_speakbtn2;
	IBOutlet UIButton *btn_speakbtn3;
	IBOutlet UIButton *btn_speakbtn4;
	IBOutlet UIButton *btn_speakbtn5;
	IBOutlet UIButton *btn_speakbtn6;	
	// Landscape Speak Time imageview
	IBOutlet UIImageView *imgview_speakls;
	
	
	
	// Portrait Speak time touch buttons
	IBOutlet UIButton *btn_speakbtn1pr;
	IBOutlet UIButton *btn_speakbtn2pr;
	IBOutlet UIButton *btn_speakbtn3pr;
	IBOutlet UIButton *btn_speakbtn4pr;
	IBOutlet UIButton *btn_speakbtn5pr;	
	// Portrait Speak Time imageview
	IBOutlet UIImageView *imgview_speakpr;
	
	
	
	// Landscape Brightness touch buttons
	IBOutlet UIButton *btn_brightbtn1;
	IBOutlet UIButton *btn_brightbtn2;	
	// Landscape Brightness imageview
	IBOutlet UIImageView *imgview_brightnessls;
	
	
	
	// Portrait Brightness touch buttons
	IBOutlet UIButton *btn_brightbtn1pr;
	IBOutlet UIButton *btn_brightbtn2pr;	
	// Portrait Brightness imageview
	IBOutlet UIImageView *imgview_brightnesspr;
	
	
	
	// Landscape Hourmode touch buttons
	IBOutlet UIButton *btn_hourmodebtn1;
	IBOutlet UIButton *btn_hourmodebtn2;
	// Landscape Hourmode imageview
	IBOutlet UIImageView *imgview_hourmode;
	
	
	
	// Portrait Hourmode touch buttons
	IBOutlet UIButton *btn_hourmodebtn1pr;
	IBOutlet UIButton *btn_hourmodebtn2pr;
	// Portrait Hourmode imageview
	IBOutlet UIImageView *imgview_hourmodepr;
	
	
	// Landscape Alarm Active touch buttons
	IBOutlet UIButton *btn_alarmactivebtn1;
	IBOutlet UIButton *btn_alarmactivebtn2;
	// Landscape Alarm Active imageview
	IBOutlet UIImageView *imgview_alarmactive;
	
	
	// Portrait Alarm Active touch buttons
	IBOutlet UIButton *btn_alarmactivebtn1pr;
	IBOutlet UIButton *btn_alarmactivebtn2pr;
	// Portrait Alarm Active imageview
	IBOutlet UIImageView *imgview_alarmactivepr;
	
	
	// Hour up touch button
	IBOutlet UIButton *btn_hourup;
	// Hour up ImageView
	IBOutlet UIImageView *imgview_hourup;
	
	
	// Hour down touch button
	IBOutlet UIButton *btn_hourdown;
	// Hour down ImageView
	IBOutlet UIImageView *imgview_hourdown;
	
	
	// Minute up touch button
	IBOutlet UIButton *btn_minuteup;
	// Minute up ImageView
	IBOutlet UIImageView *imgview_minuteup;
	
	
	// Minute down touch button
	IBOutlet UIButton *btn_minutedown;
	// Minute down ImageView
	IBOutlet UIImageView *imgview_minutedown;
	
	
	// Alarm Next and Previous buttons
	IBOutlet UIButton *btn_alarmsndnext;
	IBOutlet UIImageView *imgview_alarmsndnext;
	
	IBOutlet UIButton *btn_alarmsndprev;
	IBOutlet UIImageView *imgview_alarmsndprev;

	
	// Portrait Alarm Next and Previous buttons
	IBOutlet UIButton *btn_alarmsndnextpr;
	IBOutlet UIImageView *imgview_alarmsndnextpr;
	
	IBOutlet UIButton *btn_alarmsndprevpr;
	IBOutlet UIImageView *imgview_alarmsndprevpr;
	
	// English and Klingon Mode buttons
	IBOutlet UIButton *btn_englishmode;
	IBOutlet UIButton *btn_klingonmode;
	
	IBOutlet UIImageView *imgview_englishmode;
	IBOutlet UIImageView *imgview_klingonmode;
	
	// The language reference
	IBOutlet UIButton *btn_refonoff;
	IBOutlet UIImageView *imgview_reference;
	
	// The clock detail view
	IBOutlet ClockDetailView *view_clockdetail;
	
	// The date detail view
	IBOutlet DateDetailView *view_datedetail;
	
	// The day detail view
	IBOutlet DayDetailView *view_daydetail;
	
	// The alarm detail view
	IBOutlet AlarmDetailView *view_alarmdetail;
	
	// The enabled imageview
	IBOutlet UIImageView *imgview_enabled;
	// The off imageview
	IBOutlet UIImageView *imgview_off;
	
	// The AM and PM image views
	IBOutlet UIImageView *imgview_am;
	IBOutlet UIImageView *imgview_pm;
	
	// The alert indicator buttons and images
	IBOutlet UIButton *btn_alarmdismiss;
	IBOutlet UIButton *btn_alarmsnooze;
	IBOutlet UIImageView *imgview_alert;
	
	bool _breferenceenabled;
	
	// The clock timer
	NSTimer *_clocktimer;
	bool _ispm;
	NSLocale *_uslocale;
	NSCalendar *_gregorian;
	NSInteger _numericmonth;
	NSInteger _numericweekday;
	int _currentminute;
	int _currenthour;
	int _currenthour12;
	int _currentsecond;
	int _currentbrightness;
	NSDateFormatter* _formatter;
	bool _12hourmode;
	bool _klingonmode;

	// Alarm flags
	NSMutableString *_stralarmtime;
	bool _alarmactive;
	int _currentalarmminute;
	int _currentalarmhour;
	AlarmAdjustMode _alarmadjustmode;
	NSTimer *_alarmadjusttimer;
	bool _alarmadjustedbytimer;
	int _currentalarmsound;
	
	// Audio vars
	AVAudioPlayer* _myaudioplayer;
	bool _audioplaying;
	
	// Voice announcement vars
	bool _timeannouncementplaying;
	int _iannouncenumber;
	NSTimer *announceTimer;
	
	// Flashing alert vars
	NSTimer *alertTrigger;
	NSTimer *snoozeTrigger;
	bool _userdismissedalert;
	bool _alertactive;
	bool _alarmadjusttimeractive;
}


// PROPERTIES

@property (retain, nonatomic) UIImageView *imgview_background;

// Landscape Speak Time button properties
@property (retain, nonatomic) UIButton *btn_speakbtn1;
@property (retain, nonatomic) UIButton *btn_speakbtn2;
@property (retain, nonatomic) UIButton *btn_speakbtn3;
@property (retain, nonatomic) UIButton *btn_speakbtn4;
@property (retain, nonatomic) UIButton *btn_speakbtn5;
@property (retain, nonatomic) UIButton *btn_speakbtn6;
// Landscape Speak Time Image View
@property (retain, nonatomic) UIImageView *imgview_speakls;



// Portrait Speak Time Button properties
@property (retain, nonatomic) UIButton *btn_speakbtn1pr;
@property (retain, nonatomic) UIButton *btn_speakbtn2pr;
@property (retain, nonatomic) UIButton *btn_speakbtn3pr;
@property (retain, nonatomic) UIButton *btn_speakbtn4pr;
@property (retain, nonatomic) UIButton *btn_speakbtn5pr;
// Properties Speak Time Image VIew
@property (retain, nonatomic) UIImageView *imgview_speakpr;



// Landscape Brightness Button Properties
@property (retain, nonatomic) UIButton *btn_brightbtn1;
@property (retain, nonatomic) UIButton *btn_brightbtn2;
// Landscape Brightness Image View
@property (retain, nonatomic) UIImageView *imgview_brightnessls;




// Portrait Brightness Button Properties
@property (retain, nonatomic) UIButton *btn_brightbtn1pr;
@property (retain, nonatomic) UIButton *btn_brightbtn2pr;
// Portrait Brightness Image View
@property (retain, nonatomic) UIImageView *imgview_brightnesspr;




// Landscape Hourmode Button properties
@property (retain, nonatomic) UIButton *btn_hourmodebtn1;
@property (retain, nonatomic) UIButton *btn_hourmodebtn2;
// Landscape Hourmode ImageView
@property (retain, nonatomic) UIImageView *imgview_hourmode;




// Portrait Hourmode Button properties
@property (retain, nonatomic) UIButton *btn_hourmodebtn1pr;
@property (retain, nonatomic) UIButton *btn_hourmodebtn2pr;
// Portrait Hourmode ImageView
@property (retain, nonatomic) UIImageView *imgview_hourmodepr;



// Landscape Alarm Active Button properties
@property (retain, nonatomic) UIButton *btn_alarmactivebtn1;
@property (retain, nonatomic) UIButton *btn_alarmactivebtn2;
// Landscape Alarm Active ImageView
@property (retain, nonatomic) UIImageView *imgview_alarmactive;



// Portrait Alarm Active Button properties
@property (retain, nonatomic) UIButton *btn_alarmactivebtn1pr;
@property (retain, nonatomic) UIButton *btn_alarmactivebtn2pr;
// Portrait Alarm Active ImageView
@property (retain, nonatomic) UIImageView *imgview_alarmactivepr;


// Alarm Time buttons
@property (retain, nonatomic) UIButton *btn_hourup;
@property (retain, nonatomic) UIImageView *imgview_hourup;

@property (retain, nonatomic) UIButton *btn_hourdown;
@property (retain, nonatomic) UIImageView *imgview_hourdown;

@property (retain, nonatomic) UIButton *btn_minuteup;
@property (retain, nonatomic) UIImageView *imgview_minuteup;

@property (retain, nonatomic) UIButton *btn_minutedown;
@property (retain, nonatomic) UIImageView *imgview_minutedown;

// Alarm sound prev and next buttons

// Alarm Next and Previous buttons
@property (retain, nonatomic) UIButton *btn_alarmsndnext;
@property (retain, nonatomic) UIImageView *imgview_alarmsndnext;

@property (retain, nonatomic) UIButton *btn_alarmsndprev;
@property (retain, nonatomic) UIImageView *imgview_alarmsndprev;

// Portrait Next and Previous buttons
@property (retain, nonatomic) UIButton *btn_alarmsndnextpr;
@property (retain, nonatomic) UIImageView *imgview_alarmsndnextpr;

@property (retain, nonatomic) UIButton *btn_alarmsndprevpr;
@property (retain, nonatomic) UIImageView *imgview_alarmsndprevpr;


// English and Klingon Mode buttons
@property (retain, nonatomic) UIButton *btn_englishmode;
@property (retain, nonatomic) UIButton *btn_klingonmode;
@property (retain, nonatomic) UIImageView *imgview_englishmode;
@property (retain, nonatomic) UIImageView *imgview_klingonmode;


// The language reference and reference on off button
@property (retain, nonatomic) UIButton *btn_refonoff;
@property (retain, nonatomic) UIImageView *imgview_reference;

// The Enabled ImageView
@property (retain, nonatomic) UIImageView *imgview_enabled;
// The Off ImageView
@property (retain, nonatomic) UIImageView *imgview_off;

// The Clock Detail view
@property (retain, nonatomic) ClockDetailView *view_clockdetail;

// Date Detail View
@property (retain, nonatomic) DateDetailView *view_datedetail;

// Day Detail View
@property (retain, nonatomic) DayDetailView *view_daydetail;

// Alarm Detail View
@property (retain, nonatomic) AlarmDetailView *view_alarmdetail;

// The AM and PM image views
@property (retain, nonatomic) UIImageView *imgview_am;
@property (retain, nonatomic) UIImageView *imgview_pm;

// The alert indicator buttons and image
@property (retain, nonatomic) UIButton *btn_alarmdismiss;
@property (retain, nonatomic) UIButton *btn_alarmsnooze;
@property (retain, nonatomic) UIImageView *imgview_alert;

- (IBAction) speakButtonDown:(id)sender;
- (IBAction) speakButtonUp:(id)sender;
- (IBAction) speakButtonDownPR:(id)sender;
- (IBAction) speakButtonUpPR:(id)sender;

- (IBAction) brightnessButtonDown:(id)sender;
- (IBAction) brightnessButtonUp:(id)sender;

- (IBAction) brightnessButtonDownPR:(id)sender;
- (IBAction) brightnessButtonUpPR:(id)sender;

- (IBAction) hourmodeButtonDown:(id)sender;
- (IBAction) hourmodeButtonUp:(id)sender;

- (IBAction) hourmodeButtonDownPR:(id)sender;
- (IBAction) hourmodeButtonUpPR:(id)sender;

- (IBAction) alarmactiveButtonDown:(id)sender;
- (IBAction) alarmactiveButtonUp:(id)sender;

- (IBAction) alarmactiveButtonDownPR:(id)sender;
- (IBAction) alarmactiveButtonUpPR:(id)sender;

- (IBAction) hourupButtonDown:(id)sender;
- (IBAction) hourupButtonUp:(id)sender;

- (IBAction) hourdownButtonDown:(id)sender;
- (IBAction) hourdownButtonUp:(id)sender;

- (IBAction) minuteupButtonDown:(id)sender;
- (IBAction) minuteupButtonUp:(id)sender;

- (IBAction) minutedownButtonDown:(id)sender;
- (IBAction) minutedownButtonUp:(id)sender;


- (IBAction) alarmsoundnextButtonUp:(id)sender;
- (IBAction) alarmsoundnextButtonDown:(id)sender;
- (IBAction) alarmsoundprevButtonUp:(id)sender;
- (IBAction) alarmsoundprevButtonDown:(id)sender;

- (IBAction) alarmsoundnextButtonUpPR:(id)sender;
- (IBAction) alarmsoundnextButtonDownPR:(id)sender;
- (IBAction) alarmsoundprevButtonUpPR:(id)sender;
- (IBAction) alarmsoundprevButtonDownPR:(id)sender;

- (IBAction) englishmodeButtonDown:(id)sender;
- (IBAction) englishmodeButtonUp:(id)sender;
- (IBAction) klingonmodeButtonDown:(id)sender;
- (IBAction) klingonmodeButtonUp:(id)sender;

- (IBAction) button_refrenceOnOff:(id)sender;

- (IBAction) button_alarmsnooze:(id)sender;
- (IBAction) button_alarmdismiss:(id)sender;

- (void)setLandscapemode:(bool)landscape;
- (void)GetNumericTime;
- (void)refreshAlarmactive;
- (void) alarmAdjusttick:(id)sender;
- (void)startClocktimer;
- (void)clockTick:(id)sender;
- (void)refreshReference;

// Alarm setting stuff
- (void)MinuteAdjust:(bool)down;
- (void)HourAdjust:(bool)down;
- (void)alarmAdjusttick:(id)sender;
- (void)startAlarmadjust;
- (void)RefreshAlarmtime;
- (void)alarmAdjust:(AlarmAdjustMode)adjustmode;

- (void)refreshAmPm;
- (void)Adjustbrightness;

// Sound playing
- (void) configureAudioServices;
- (void)playSinglesound:(NSString *)resourcename;
- (void)playSinglesoundEx:(NSString *)resourcename;
- (void)playClockbuttonsound;
- (void)playAlarmbuttonsound;

// Voice announcement
- (void)announcebuttonPressed;
- (int) getsingleNumeric:(int)clocknumeric:(int)digit;
- (void)triggerAnnounce:(id)sender;
- (void)startAnnouncementTimer;
- (void)playNumericsound:(int)numeric;
- (void)playKlingonnumeral:(int)numeric;
- (void)play12Hournumeric:(int)numeric;

// Flashing alert (when the alarm is going off)
- (void)activateAlert;
- (void)deactivateAlert;
- (void)alertTrigger:(id)sender;
- (void)activateSnooze;
- (void)snoozeTrigger:(id)sender;
- (void)playCurrentalarm;

// Loading and Saving
- (void)saveSettings;
- (void)loadSettings;

// Local Notification Stuff
- (void)scheduleGenericalarm;
- (void)clearAllLocalnotifications;
- (void)scheduleAlarmForDate:(NSDate*)theDate;


@end
