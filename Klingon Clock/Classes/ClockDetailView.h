//
//  ClockDetailView.h
//  Klingon Clock
//
//  Created by Danny Draper on 29/12/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClockNumbers.h"

@interface ClockDetailView : UIView {
	
	ClockNumbers *_clocknumbers;
	ClockNumbers *_clocknumberspr;
	ClockNumbers *_secondnumbers;
	
	bool _blandscapemode;
}

- (void)setLandscapemode:(bool)landscape;
- (void)setClockstring:(NSString *)clockstring;
- (void)setSecondstring:(NSString *)secondstring;
- (void)refreshDisplay;
- (void)setKlingonmode:(bool)klingonmode;

@end
