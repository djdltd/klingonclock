//
//  ClockView.m
//  Klingon Clock
//
//  Created by Danny Draper on 20/12/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

/*
 Things to do:
 Get Klingon Speech mode working
 Fix the graphical glitches on clock numbers 3 and 8
 Fix the number 7 graphic (should not look like a 1)
 Get saving and loading working
 Get the alarm working whilst in suspend (same as iPhone version)
 
 */

#import "ClockView.h"


@implementation ClockView

@synthesize imgview_background;

@synthesize btn_speakbtn1;
@synthesize btn_speakbtn2;
@synthesize btn_speakbtn3;
@synthesize btn_speakbtn4;
@synthesize btn_speakbtn5;
@synthesize btn_speakbtn6;
@synthesize imgview_speakls;


@synthesize btn_speakbtn1pr;
@synthesize btn_speakbtn2pr;
@synthesize btn_speakbtn3pr;
@synthesize btn_speakbtn4pr;
@synthesize btn_speakbtn5pr;
@synthesize imgview_speakpr;


@synthesize btn_brightbtn1;
@synthesize btn_brightbtn2;
@synthesize imgview_brightnessls;


@synthesize btn_brightbtn1pr;
@synthesize btn_brightbtn2pr;
@synthesize imgview_brightnesspr;



@synthesize btn_hourmodebtn1;
@synthesize btn_hourmodebtn2;
@synthesize imgview_hourmode;


@synthesize btn_hourmodebtn1pr;
@synthesize btn_hourmodebtn2pr;
@synthesize imgview_hourmodepr;


@synthesize btn_alarmactivebtn1;
@synthesize btn_alarmactivebtn2;
@synthesize imgview_alarmactive;


@synthesize btn_alarmactivebtn1pr;
@synthesize btn_alarmactivebtn2pr;
@synthesize imgview_alarmactivepr;

// Alarm setting buttons
@synthesize btn_hourup;
@synthesize imgview_hourup;

@synthesize btn_hourdown;
@synthesize imgview_hourdown;

@synthesize btn_minuteup;
@synthesize imgview_minuteup;

@synthesize btn_minutedown;
@synthesize imgview_minutedown;


// Alarm sound prev and next buttons
@synthesize btn_alarmsndnext;
@synthesize imgview_alarmsndnext;

@synthesize btn_alarmsndprev;
@synthesize imgview_alarmsndprev;

// Portrait sound prev and next buttons
@synthesize btn_alarmsndnextpr;
@synthesize imgview_alarmsndnextpr;

@synthesize btn_alarmsndprevpr;
@synthesize imgview_alarmsndprevpr;


// English and Klingon Mode buttons
@synthesize btn_englishmode;
@synthesize btn_klingonmode;
@synthesize imgview_englishmode;
@synthesize imgview_klingonmode;
// Reference on and off button
@synthesize btn_refonoff;
@synthesize imgview_reference;
// Clock detail view
@synthesize view_clockdetail;

// Date Detail View
@synthesize view_datedetail;

// Day Detail View
@synthesize view_daydetail;

// The Alarm Detail View
@synthesize view_alarmdetail;

// The enabled image view
@synthesize imgview_enabled;
// The off image view
@synthesize imgview_off;

// The AM and PM image views
@synthesize imgview_am;
@synthesize imgview_pm;

// The alert indicator buttons and image
@synthesize btn_alarmdismiss;
@synthesize btn_alarmsnooze;
@synthesize imgview_alert;

- (id)initWithCoder:(NSCoder *)coder
{
	if ((self = [super initWithCoder:coder])) {
		NSLog (@"Init with coder has been called!");	
		
		_breferenceenabled = true;
		_alarmactive = false;
		_klingonmode = false;
		_12hourmode = false;
		
		_userdismissedalert = false;
		_alertactive = false;
		_alarmadjusttimeractive = false;
		
		_currentbrightness = 5;
		_currentalarmsound = 1;
		
		_currentalarmhour = 7;
		_currentalarmminute = 30;
		
		[self loadSettings];
		
		[_stralarmtime = [[NSMutableString alloc] init] retain];
		[_gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] retain];
		[_formatter = [[NSDateFormatter alloc] init] retain];
		
		[_uslocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] retain];
		[_formatter setLocale:_uslocale];
		
		[self configureAudioServices];

		[self startClocktimer];
		
		[self refreshReference];
		
	}

	
	return self;
}

- (void)saveSettings
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	
	[prefs setBool:_alarmactive forKey:@"alarmactive"];
	[prefs setBool:_12hourmode forKey:@"12hourmode"];
	[prefs setInteger:_currentalarmsound forKey:@"currentalarmsound"];
	[prefs setInteger:_currentalarmhour forKey:@"currentalarmhour"];
	[prefs setInteger:_currentalarmminute forKey:@"currentalarmminute"];
	[prefs setBool:true forKey:@"settingspresent"];
	[prefs setBool:_breferenceenabled forKey:@"referenceenabled"];
	[prefs synchronize];
}

- (void)loadSettings
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	
	bool settingspresent = [prefs boolForKey:@"settingspresent"];
	
	if (settingspresent == true) {
		
		_alarmactive = [prefs boolForKey:@"alarmactive"];
		_12hourmode = [prefs boolForKey:@"12hourmode"];
		_currentalarmsound = [prefs integerForKey:@"currentalarmsound"];
		_currentalarmhour = [prefs integerForKey:@"currentalarmhour"];
		_currentalarmminute = [prefs integerForKey:@"currentalarmminute"];
		_breferenceenabled = [prefs boolForKey:@"referenceenabled"];
	}
	
	if (_currentalarmsound == 0) {
		_currentalarmsound = 1;
	}
}

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
    }
    return self;
}


- (IBAction) speakButtonDown:(id)sender
{
	imgview_speakls.alpha = 0.5f;
}

- (IBAction) speakButtonUp:(id)sender
{
	[self playClockbuttonsound];
	[self announcebuttonPressed];
	imgview_speakls.alpha = 1.0f;
}

- (IBAction) speakButtonDownPR:(id)sender
{
	imgview_speakpr.alpha = 0.5f;
}

- (IBAction) speakButtonUpPR:(id)sender
{
	[self playClockbuttonsound];
	[self announcebuttonPressed]; 
	imgview_speakpr.alpha = 1.0f;
}

- (IBAction) brightnessButtonDown:(id)sender
{
	imgview_brightnessls.alpha = 0.5f;
}

- (IBAction) brightnessButtonUp:(id)sender
{
	[self playClockbuttonsound];	
	[self Adjustbrightness];
	imgview_brightnessls.alpha = 1.0f;
}

- (IBAction) brightnessButtonDownPR:(id)sender
{
	imgview_brightnesspr.alpha = 0.5f;
}


- (IBAction) brightnessButtonUpPR:(id)sender
{
	[self playClockbuttonsound];	
	[self Adjustbrightness];
	imgview_brightnesspr.alpha = 1.0f;
}

- (void) Adjustbrightness
{
	if (_currentbrightness < 5) {
		_currentbrightness++;
	} else {
		_currentbrightness = 1;
	}
	
	if (_currentbrightness == 1) {
		self.alpha = 0.2f;
	}
	
	if (_currentbrightness == 2) {
		self.alpha = 0.4f;
	}
	
	if (_currentbrightness == 3) {
		self.alpha = 0.6f;
	}
	
	if (_currentbrightness == 4) {
		self.alpha = 0.8f;
	}
	
	if (_currentbrightness == 5) {
		self.alpha = 1.0f;
	}
}


- (IBAction) hourmodeButtonDown:(id)sender
{
	imgview_hourmode.alpha = 0.5f;
}


- (IBAction) hourmodeButtonUp:(id)sender
{
	[self playClockbuttonsound];	
	imgview_hourmode.alpha = 1.0f;
	if (_12hourmode == false) {
		_12hourmode = true;
	} else {
		_12hourmode = false;
	}
}


- (IBAction) hourmodeButtonDownPR:(id)sender
{
	imgview_hourmodepr.alpha = 0.5f;
}


- (IBAction) hourmodeButtonUpPR:(id)sender
{
	[self playClockbuttonsound];	
	imgview_hourmodepr.alpha = 1.0f;
	if (_12hourmode == false) {
		_12hourmode = true;
	} else {
		_12hourmode = false;
	}
}

- (IBAction) alarmactiveButtonDown:(id)sender
{
	imgview_alarmactive.alpha = 0.5f;
}

- (IBAction) alarmactiveButtonUp:(id)sender
{
	[self playClockbuttonsound];	
	if (_alarmactive == true) {
		_alarmactive = false;
		[self refreshAlarmactive];
	} else {
		_alarmactive = true;
		[self refreshAlarmactive];
	}
	
	imgview_alarmactive.alpha = 1.0f;
}

- (IBAction) alarmactiveButtonDownPR:(id)sender
{
	imgview_alarmactivepr.alpha = 0.5f;
}

- (IBAction) alarmactiveButtonUpPR:(id)sender
{
	[self playClockbuttonsound];	
	if (_alarmactive == true) {
		_alarmactive = false;
		[self refreshAlarmactive];
	} else {
		_alarmactive = true;
		[self refreshAlarmactive];
	}
	
	imgview_alarmactivepr.alpha = 1.0f;
}


// Alarm Setting buttons
- (IBAction) hourupButtonDown:(id)sender
{
	imgview_hourup.alpha = 0.5f;
	_alarmadjustmode = AdjHourUp;
	[self startAlarmadjust];
}

- (IBAction) hourupButtonUp:(id)sender
{
	[self playAlarmbuttonsound];	
	imgview_hourup.alpha = 1.0f;
	[self alarmAdjust:AdjHourUp];
}

- (IBAction) hourdownButtonDown:(id)sender
{
	imgview_hourdown.alpha = 0.5f;
	_alarmadjustmode = AdjHourDown;
	[self startAlarmadjust];	
}

- (IBAction) hourdownButtonUp:(id)sender
{
	[self playAlarmbuttonsound];		
	imgview_hourdown.alpha = 1.0f;
	[self alarmAdjust:AdjHourDown];
}

- (IBAction) minuteupButtonDown:(id)sender
{
	imgview_minuteup.alpha = 0.5f;
	_alarmadjustmode = AdjMinuteUp;
	[self startAlarmadjust];
}

- (IBAction) minuteupButtonUp:(id)sender
{
	[self playAlarmbuttonsound];		
	imgview_minuteup.alpha = 1.0f;
	[self alarmAdjust:AdjMinuteUp];
}

- (IBAction) minutedownButtonDown:(id)sender
{
	imgview_minutedown.alpha = 0.5f;
	_alarmadjustmode = AdjMinuteDown;
	[self startAlarmadjust];
}

- (IBAction) minutedownButtonUp:(id)sender
{
	[self playAlarmbuttonsound];		
	imgview_minutedown.alpha = 1.0f;
	[self alarmAdjust:AdjMinuteDown];
}

- (IBAction) alarmsoundnextButtonUp:(id)sender
{
	_currentalarmsound++;
	
	if (_currentalarmsound > 10) {
		_currentalarmsound = 1;
	}
	
	
	[view_alarmdetail setAlarmSound:_currentalarmsound];
	[self playCurrentalarm];
	
	imgview_alarmsndnext.alpha = 1.0f;
}

- (IBAction) alarmsoundnextButtonDown:(id)sender
{
	imgview_alarmsndnext.alpha = 0.5f;
}

- (IBAction) alarmsoundprevButtonUp:(id)sender
{
	_currentalarmsound--;
	
	if (_currentalarmsound < 1) {
		_currentalarmsound = 10;
	}

	[view_alarmdetail setAlarmSound:_currentalarmsound];	
	[self playCurrentalarm];	
	
	imgview_alarmsndprev.alpha = 1.0f;
}

- (IBAction) alarmsoundprevButtonDown:(id)sender
{
	imgview_alarmsndprev.alpha = 0.5f;
}


- (IBAction) alarmsoundnextButtonUpPR:(id)sender
{
	_currentalarmsound++;
	
	if (_currentalarmsound > 10) {
		_currentalarmsound = 1;
	}
	
	[view_alarmdetail setAlarmSound:_currentalarmsound];	
	[self playCurrentalarm];
	
	imgview_alarmsndnextpr.alpha = 1.0f;
}

- (IBAction) alarmsoundnextButtonDownPR:(id)sender
{
	imgview_alarmsndnextpr.alpha = 0.5f;
}

- (IBAction) alarmsoundprevButtonUpPR:(id)sender
{
	_currentalarmsound--;
	
	if (_currentalarmsound < 1) {
		_currentalarmsound = 10;
	}
	
	[view_alarmdetail setAlarmSound:_currentalarmsound];	
	[self playCurrentalarm];
	
	imgview_alarmsndprevpr.alpha = 1.0f;
}

- (IBAction) alarmsoundprevButtonDownPR:(id)sender
{
	imgview_alarmsndprevpr.alpha = 0.5f;
}

- (IBAction) englishmodeButtonDown:(id)sender
{
	imgview_englishmode.alpha = 0.5f;
}

- (IBAction) englishmodeButtonUp:(id)sender
{
	[self playClockbuttonsound];		
	_klingonmode = false;
	imgview_englishmode.alpha = 1.0f;
	[view_clockdetail setKlingonmode:false];
	[view_daydetail setKlingonmode:false];
	[view_datedetail setKlingonmode:false];
	[view_alarmdetail setKlingonmode:false];
	[view_clockdetail refreshDisplay];
}

- (IBAction) klingonmodeButtonDown:(id)sender
{
	imgview_klingonmode.alpha = 0.5f;
}

- (IBAction) klingonmodeButtonUp:(id)sender
{
	[self playClockbuttonsound];	
	_klingonmode = true;
	imgview_klingonmode.alpha = 1.0f;
	[view_clockdetail setKlingonmode:true];
	[view_daydetail setKlingonmode:true];
	[view_datedetail setKlingonmode:true];
	[view_alarmdetail setKlingonmode:true];
	[view_clockdetail refreshDisplay];
}

- (IBAction) button_refrenceOnOff:(id)sender
{
	[self playClockbuttonsound];	
	if (_breferenceenabled == true) {
		_breferenceenabled = false;
		imgview_reference.hidden = true;
	} else {
		_breferenceenabled = true;
		imgview_reference.hidden = false;
	
	}
}

- (IBAction) button_alarmsnooze:(id)sender
{
	[self playClockbuttonsound];	
	[self deactivateAlert];
	[self activateSnooze];
}


- (IBAction) button_alarmdismiss:(id)sender
{
	[self playClockbuttonsound];	
	[self deactivateAlert];
}

- (void)refreshAlarmactive
{
	if (_alarmactive == true) {
		imgview_enabled.hidden = false;
		imgview_off.hidden = true;
	} else {
		imgview_enabled.hidden = true;
		imgview_off.hidden = false;
	}
}

- (void)refreshReference
{
	if (_breferenceenabled == true) {
		imgview_reference.hidden = false;
	} else {
		imgview_reference.hidden = true;
	}
	
	//NSLog (@"Reference enabled: %@", _breferenceenabled);
	[self setNeedsDisplay];
}

/////////////////////////////////////////////////////////////////////////
// ALARM SETTING STUFF
/////////////////////////////////////////////////////////////////////////

- (void)refreshAmPm
{
	if (_klingonmode == false) {
		if (_12hourmode == true) {	
			if (_ispm == true) {
				imgview_am.hidden = true;
				imgview_pm.hidden = false;
			} else {
				imgview_am.hidden = false;
				imgview_pm.hidden = true;
			}
		} else {
			imgview_am.hidden = true;
			imgview_pm.hidden = true;
		} 
	} else {
		imgview_am.hidden = true;
		imgview_pm.hidden = true;
	}
}

- (void)RefreshAlarmtime
{
	NSNumber *mins;
	NSNumber *hours;
	
	[_stralarmtime setString:@""];
	
	mins = [NSNumber numberWithInt:_currentalarmminute];
	hours = [NSNumber numberWithInt:_currentalarmhour];
	
	if ([hours intValue] < 10) {
		[_stralarmtime appendString:@"0"];
	}
	
	[_stralarmtime appendString:[hours stringValue]];
	[_stralarmtime appendString:@":"];
	
	if ([mins intValue] < 10) {
		[_stralarmtime appendString:@"0"];
	}
	
	[_stralarmtime appendString:[mins stringValue]];
	
	[view_alarmdetail setAlarmString:_stralarmtime];
	
}

- (void)MinuteAdjust:(bool)down
{
	if (down == true) {
		_currentalarmminute--;
		
		if (_currentalarmminute <= -1) {
			_currentalarmminute = 59;
		}
			
	} else {
		++_currentalarmminute;
			
		if (_currentalarmminute >= 60) {
			_currentalarmminute = 0;
		}
	}
		
	[self RefreshAlarmtime];
	[self setNeedsDisplay];
}


- (void)HourAdjust:(bool)down
{

	if (down == true) {
		_currentalarmhour--;
		
		if (_currentalarmhour <= -1) {
			_currentalarmhour = 23;
		}
		
	} else {
		++_currentalarmhour;
			
		if (_currentalarmhour >= 24) {
			_currentalarmhour = 0;
		}
	}
		
	[self RefreshAlarmtime];
	[self setNeedsDisplay];
	
}

- (void)alarmAdjust:(AlarmAdjustMode)adjustmode
{
	if (_alarmadjustedbytimer == false) {
		if (adjustmode == AdjHourUp) {
			[self HourAdjust:false];
		}
		if (adjustmode == AdjHourDown) {
			[self HourAdjust:true];
		}
		if (adjustmode == AdjMinuteUp) {
			[self MinuteAdjust:false];
		}
		if (adjustmode == AdjMinuteDown) {
			[self MinuteAdjust:true];
		}
		
	}
	
	if (_alarmadjusttimer != nil) {
		[_alarmadjusttimer invalidate];
		[_alarmadjusttimer release];
		_alarmadjusttimer = nil;
		_alarmadjusttimeractive = false;
	}
}

- (void) alarmAdjusttick:(id)sender
{
	if (_alarmadjustmode == AdjHourUp)
	{
		_alarmadjustedbytimer = true;
		[self HourAdjust:false];
	}
	
	if (_alarmadjustmode == AdjHourDown)
	{
		_alarmadjustedbytimer = true;
		[self HourAdjust:true];
	}
	
	if (_alarmadjustmode == AdjMinuteUp)
	{
		_alarmadjustedbytimer = true;
		[self MinuteAdjust:false];
	}
	
	if (_alarmadjustmode == AdjMinuteDown)
	{
		_alarmadjustedbytimer = true;
		[self MinuteAdjust:true];
	}
}

- (void)startAlarmadjust
{
	if (_alarmadjusttimeractive == false) {
	
		_alarmadjusttimeractive = true;
		//[self HourAdjust:false];
		_alarmadjustedbytimer = false;
		_alarmadjusttimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)0.3 target:self selector:@selector(alarmAdjusttick:) userInfo:nil repeats:TRUE];
		[_alarmadjusttimer retain];
	}
}

/////////////////////////////////////////////////////////////////////////
// ORIENTATION - REPOSITIONING AND RESIZING
/////////////////////////////////////////////////////////////////////////


- (void)setLandscapemode:(bool)landscape
{
	if (landscape == true) {
		NSLog (@"Landscape mode!");
		
		[imgview_background setImage:[UIImage imageNamed:@"BackgroundLandscape.png"]];
		
		// Show the landscape speak time button
		
		imgview_speakls.hidden = false;
		btn_speakbtn1.hidden = false;
		btn_speakbtn2.hidden = false;
		btn_speakbtn3.hidden = false;
		btn_speakbtn4.hidden = false;
		btn_speakbtn5.hidden = false;
		btn_speakbtn6.hidden = false;
		
		// Hide the portrait speak time button
		imgview_speakpr.hidden = true;
		btn_speakbtn1pr.hidden = true;
		btn_speakbtn2pr.hidden = true;
		btn_speakbtn3pr.hidden = true;
		btn_speakbtn4pr.hidden = true;
		btn_speakbtn5pr.hidden = true;
		
		// Show the landscape brightness button
		imgview_brightnessls.hidden = false;
		btn_brightbtn1.hidden = false;
		btn_brightbtn2.hidden = false;
		
		// Hide the portrait brightness button
		imgview_brightnesspr.hidden = true;
		btn_brightbtn1pr.hidden = true;
		btn_brightbtn2pr.hidden = true;
		
		// Show the landscape hourmode button
		imgview_hourmode.hidden = false;
		btn_hourmodebtn1.hidden = false;
		btn_hourmodebtn2.hidden = false;
		
		// Hide the portrait hourmode button
		imgview_hourmodepr.hidden = true;
		btn_hourmodebtn1pr.hidden = true;
		btn_hourmodebtn2pr.hidden = true;
		
		// Show the landscape alarm active button
		imgview_alarmactive.hidden = false;
		btn_alarmactivebtn1.hidden = false;
		btn_alarmactivebtn2.hidden = false;
		
		// Hide the portrait alarm active button
		imgview_alarmactivepr.hidden = true;
		btn_alarmactivebtn1pr.hidden = true;
		btn_alarmactivebtn2pr.hidden = true;
		
		// Reposition the alarm setting buttons
		imgview_hourup.frame = CGRectMake (156.0f, 642.0f, 80.0f, 45.0f);
		imgview_hourdown.frame = CGRectMake (222.0f, 642.0f, 80.0f, 45.0f);
		imgview_minuteup.frame = CGRectMake (451.0f, 642.0f, 80.0f, 45.0f);
		imgview_minutedown.frame = CGRectMake (517.0f, 642.0f, 80.0f, 45.0f);
		
		btn_hourup.frame = CGRectMake (172.0f ,642.0f, 47.0f, 44.0f);
		btn_hourdown.frame = CGRectMake (238.0f, 642.0f, 47.0f, 44.0f);
		btn_minuteup.frame = CGRectMake (467.0f, 642.0f, 47.0f, 44.0f);
		btn_minutedown.frame = CGRectMake (533.0f, 642.0f, 47.0f, 44.0f);
		
		// Show the landscape alarm prev and next buttons
		btn_alarmsndnext.hidden = false;
		btn_alarmsndprev.hidden = false;
		imgview_alarmsndnext.hidden = false;
		imgview_alarmsndprev.hidden = false;
		
		// Hide the portrait alarm prev and next buttons
		btn_alarmsndnextpr.hidden = true;
		btn_alarmsndprevpr.hidden = true;
		imgview_alarmsndnextpr.hidden = true;
		imgview_alarmsndprevpr.hidden = true;
		
		// Reposition the english and klingon mode buttons
		//landscape
		btn_englishmode.frame = CGRectMake (620.0f, 668.0f, 57.0f, 45.0f);
		btn_klingonmode.frame = CGRectMake (704.0f, 668.0f, 57.0f, 45.0f);
		imgview_englishmode.frame = CGRectMake (600.0f, 668.0f, 96.0f, 45.0f);
		imgview_klingonmode.frame = CGRectMake (684.0f, 668.0f, 96.0f, 45.0f);
		
		// the alarm enabled and off label
		imgview_enabled.frame = CGRectMake (367.0f, 319.0f, 158.0f, 35.0f);
		imgview_off.frame = CGRectMake (409.0f, 320.0f, 74.0f, 34.0f);
		
		// Reposition the language reference button and reference image
		btn_refonoff.frame = CGRectMake (31.0f, 640.0f, 110.0f, 66.0f);
		imgview_reference.frame = CGRectMake (144.0f, 572.0f, 280.0f, 52.0f);
		
		// Reposition the Clock Detail view
		view_clockdetail.frame = CGRectMake (185.0f, 364.0f, 700.0f, 257.0f);
		
		// Reposition the Date and Day views for landscape
		view_datedetail.frame = CGRectMake (213.0f, 126.0f, 608.0f, 139.0f);
		view_daydetail.frame = CGRectMake (252.0f, 52.0f, 519.0f, 66.0f);
		
		// Reposition the Alarm Detail view
		view_alarmdetail.frame = CGRectMake (541.0f, 310.0f, 306.0f, 56.0f);

		// Reposition the AM and PM imageviews
		imgview_am.frame = CGRectMake (768.0f, 575.0f, 79.0f, 37.0f);
		imgview_pm.frame = CGRectMake (768.0f, 575.0f, 79.0f, 37.0f);
		
		// Reposition the alert buttons and image
		btn_alarmdismiss.frame = CGRectMake (260.0f, 77.0f, 118.0f, 152.0f);
		btn_alarmsnooze.frame = CGRectMake (372.0f, 78.0f, 118.0f, 152.0f);
		imgview_alert.frame = CGRectMake (476.0f, 118.0f, 255.0f, 69.0f);
		
		// Tell the subviews stuff needs to change
		[view_clockdetail setLandscapemode:landscape];
		[view_daydetail setLandscapemode:landscape];
		[view_datedetail setLandscapemode:landscape];
		
	} else {
		NSLog (@"Portrait mode!");
		
		[imgview_background setImage:[UIImage imageNamed:@"BackgroundPortrait.png"]];
		
		// Hide the landscape speak time button
		imgview_speakls.hidden = true;
		btn_speakbtn1.hidden = true;
		btn_speakbtn2.hidden = true;
		btn_speakbtn3.hidden = true;
		btn_speakbtn4.hidden = true;
		btn_speakbtn5.hidden = true;
		btn_speakbtn6.hidden = true;
		
		
		// Show the portrait speak time button
		imgview_speakpr.hidden = false;
		btn_speakbtn1pr.hidden = false;
		btn_speakbtn2pr.hidden = false;
		btn_speakbtn3pr.hidden = false;
		btn_speakbtn4pr.hidden = false;
		btn_speakbtn5pr.hidden = false;		
		
		// Hide the landscape brightness button
		imgview_brightnessls.hidden = true;
		btn_brightbtn1.hidden = true;
		btn_brightbtn2.hidden = true;
		
		// Show the portrait brightness button
		imgview_brightnesspr.hidden = false;
		btn_brightbtn1pr.hidden = false;
		btn_brightbtn2pr.hidden = false;
		
		
		// Hide the landscape hourmode button
		imgview_hourmode.hidden = true;
		btn_hourmodebtn1.hidden = true;
		btn_hourmodebtn2.hidden = true;
		
		// Show the portrait hourmode button
		imgview_hourmodepr.hidden = false;
		btn_hourmodebtn1pr.hidden = false;
		btn_hourmodebtn2pr.hidden = false;
		
		
		// Hide the landscape alarm active button
		imgview_alarmactive.hidden = true;
		btn_alarmactivebtn1.hidden = true;
		btn_alarmactivebtn2.hidden = true;
		
		// Show the portrait alarm active button
		imgview_alarmactivepr.hidden = false;
		btn_alarmactivebtn1pr.hidden = false;
		btn_alarmactivebtn2pr.hidden = false;
		
		
		// Reposition the alarm setting buttons
		imgview_hourup.frame = CGRectMake (121.0f, 910.0f, 80.0f, 45.0f);
		imgview_hourdown.frame = CGRectMake (187.0f, 910.0f, 80.0f, 45.0f);
		imgview_minuteup.frame = CGRectMake (416.0f, 910.0f, 80.0f, 45.0f);
		imgview_minutedown.frame = CGRectMake (482.0f, 910.0f, 80.0f, 45.0f);
		
		btn_hourup.frame = CGRectMake (137.0f, 910.0f, 47.0f, 44.0f);
		btn_hourdown.frame = CGRectMake (203.0f, 910.0f, 47.0f, 44.0f);
		btn_minuteup.frame = CGRectMake (432.0f, 910.0f, 47.0f, 44.0f);
		btn_minutedown.frame = CGRectMake (498.0f, 910.0f, 47.0f, 44.0f);
		
		// Reposition the english and klingon mode buttons
		// portrait
		btn_englishmode.frame = CGRectMake (586.0f, 924.0f, 57.0f, 45.0f);
		btn_klingonmode.frame = CGRectMake (670.0f, 924.0f, 57.0f, 45.0f);
		imgview_englishmode.frame = CGRectMake (566.0f, 924.0f, 96.0f, 45.0f);
		imgview_klingonmode.frame = CGRectMake (650.0f, 924.0f, 96.0f, 45.0f);
		
		
		// Hide the landscape alarm prev and next buttons
		btn_alarmsndnext.hidden = true;
		btn_alarmsndprev.hidden = true;
		imgview_alarmsndnext.hidden = true;
		imgview_alarmsndprev.hidden = true;
		
		// Show the portrait alarm prev and next buttons
		btn_alarmsndnextpr.hidden = false;
		btn_alarmsndprevpr.hidden = false;
		imgview_alarmsndnextpr.hidden = false;
		imgview_alarmsndprevpr.hidden = false;
		
		// the alarm enabled and off label
		imgview_enabled.frame = CGRectMake (189.0f, 433.0f, 158.0f, 35.0f);
		imgview_off.frame = CGRectMake (230.0f, 437.0f, 74.0f, 34.0f);
		
		// Reposition the language reference
		btn_refonoff.frame = CGRectMake (7.0f, 900.0f, 110.0f, 66.0f);
		imgview_reference.frame = CGRectMake (114.0f, 839.0f, 280.0f, 52.0f);
		
		// Reposition the Clock Detail view
		view_clockdetail.frame = CGRectMake (65.0f, 489.0f, 637.0f, 276.0f);
		
		// Reposition the Date and Day views for portrait
		view_datedetail.frame = CGRectMake (137.0f, 226.0f, 504.0f, 139.0f);
		view_daydetail.frame = CGRectMake (187.0f, 90.0f, 397.0f, 66.0f);
		
		// Reposition the Alarm Detail view
		view_alarmdetail.frame = CGRectMake (365.0f, 425.0f, 306.0f, 56.0f);
		
		// Reposition the AM and PM imageviews
		imgview_am.frame = CGRectMake (552.0f, 757.0f, 79.0f, 37.0f);
		imgview_pm.frame = CGRectMake (552.0f, 757.0f, 79.0f, 37.0f);
		
		// Reposition the alert buttons and image
		btn_alarmdismiss.frame = CGRectMake (151.0f, 158.0f, 118.0f, 152.0f);
		btn_alarmsnooze.frame = CGRectMake (263.0f, 159.0f, 118.0f, 152.0f);
		imgview_alert.frame = CGRectMake (367.0f, 199.0f, 255.0f, 69.0f);
		
		// Resposition the sub views for portrait
		[view_clockdetail setLandscapemode:landscape];
		[view_daydetail setLandscapemode:landscape];
		[view_datedetail setLandscapemode:landscape];
	}
}

/////////////////////////////////////////////////////////////////////////
// CLOCK DISPLAY
/////////////////////////////////////////////////////////////////////////


- (void)GetNumericTime
{
	NSDate* date = [NSDate date];
	NSDateComponents *weekdayComponents =[_gregorian components:(NSDayCalendarUnit | NSWeekdayCalendarUnit) fromDate:date];
	
	//[_formatter setDateFormat:@"HH mm p"];
	//NSString* strtesttime = [_formatter stringFromDate:date];
	//NSLog (@"Test time: %@", strtesttime);
	
	[_formatter setDateFormat:@"HH"];
	NSString* strhour = [_formatter stringFromDate:date];
	
	[_formatter setDateFormat:@"mm"];
	NSString* strminute = [_formatter stringFromDate:date];
	
	[_formatter setDateFormat:@"ss"];
	NSString* strsecond = [_formatter stringFromDate:date];
	
	[_formatter setDateFormat:@"MM"];
	NSString* strmonth = [_formatter stringFromDate:date];
	
	_currentminute = [strminute intValue];
	_currenthour = [strhour intValue];
	_currentsecond = [strsecond intValue];
	_numericweekday = [weekdayComponents weekday];
	_numericmonth = [strmonth intValue];

	
	if (_currenthour == 13) {_currenthour12 = 1;}
	if (_currenthour == 14) {_currenthour12 = 2;}
	if (_currenthour == 15) {_currenthour12 = 3;}
	if (_currenthour == 16) {_currenthour12 = 4;}
	if (_currenthour == 17) {_currenthour12 = 5;}
	if (_currenthour == 18) {_currenthour12 = 6;}
	if (_currenthour == 19) {_currenthour12 = 7;}
	if (_currenthour == 20) {_currenthour12 = 8;}
	if (_currenthour == 21) {_currenthour12 = 9;}
	if (_currenthour == 22) {_currenthour12 = 10;}
	if (_currenthour == 23) {_currenthour12 = 11;}
	if (_currenthour == 0) {_currenthour12 = 12;}
	if (_currenthour == 1) {_currenthour12 = 1;}
	if (_currenthour == 2) {_currenthour12 = 2;}
	if (_currenthour == 3) {_currenthour12 = 3;}
	if (_currenthour == 4) {_currenthour12 = 4;}
	if (_currenthour == 5) {_currenthour12 = 5;}
	if (_currenthour == 6) {_currenthour12 = 6;}
	if (_currenthour == 7) {_currenthour12 = 7;}
	if (_currenthour == 8) {_currenthour12 = 8;}
	if (_currenthour == 9) {_currenthour12 = 9;}
	if (_currenthour == 10) {_currenthour12 = 10;}
	if (_currenthour == 11) {_currenthour12 = 11;}
	if (_currenthour == 12) {_currenthour12 = 12;}
	
	if (_currenthour == 0) {_ispm = false;}
	if (_currenthour == 1) {_ispm = false;}
	if (_currenthour == 2) {_ispm = false;}
	if (_currenthour == 3) {_ispm = false;}
	if (_currenthour == 4) {_ispm = false;}
	if (_currenthour == 5) {_ispm = false;}
	if (_currenthour == 6) {_ispm = false;}
	if (_currenthour == 7) {_ispm = false;}
	if (_currenthour == 8) {_ispm = false;}
	if (_currenthour == 9) {_ispm = false;}
	if (_currenthour == 10) {_ispm = false;}
	if (_currenthour == 11) {_ispm = false;}
	if (_currenthour == 12) {_ispm = true;}
	if (_currenthour == 13) {_ispm = true;}
	if (_currenthour == 14) {_ispm = true;}
	if (_currenthour == 15) {_ispm = true;}
	if (_currenthour == 16) {_ispm = true;}
	if (_currenthour == 17) {_ispm = true;}
	if (_currenthour == 18) {_ispm = true;}
	if (_currenthour == 19) {_ispm = true;}
	if (_currenthour == 20) {_ispm = true;}
	if (_currenthour == 21) {_ispm = true;}
	if (_currenthour == 22) {_ispm = true;}
	if (_currenthour == 23) {_ispm = true;}
}

- (void)startClocktimer
{
	_clocktimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)0.5 target:self selector:@selector(clockTick:) userInfo:nil repeats:TRUE];
}

- (void)clockTick:(id)sender
{
	[self GetNumericTime];
	
	NSDate* date = [NSDate date];
	[self refreshAlarmactive];
	
	if (_12hourmode == true) {
		[_formatter setDateFormat:@"HH:mm"];
		
		if (_currenthour == 13) {[_formatter setDateFormat:@" 1:mm"];}
		if (_currenthour == 14) {[_formatter setDateFormat:@" 2:mm"];}		
		if (_currenthour == 15) {[_formatter setDateFormat:@" 3:mm"];}
		if (_currenthour == 16) {[_formatter setDateFormat:@" 4:mm"];}
		if (_currenthour == 17) {[_formatter setDateFormat:@" 5:mm"];}
		if (_currenthour == 18) {[_formatter setDateFormat:@" 6:mm"];}
		if (_currenthour == 19) {[_formatter setDateFormat:@" 7:mm"];}
		if (_currenthour == 20) {[_formatter setDateFormat:@" 8:mm"];}
		if (_currenthour == 21) {[_formatter setDateFormat:@" 9:mm"];}
		if (_currenthour == 22) {[_formatter setDateFormat:@"10:mm"];}
		if (_currenthour == 23) {[_formatter setDateFormat:@"11:mm"];}
		if (_currenthour == 0) {[_formatter setDateFormat:@"12:mm"];}
		if (_currenthour == 1) {[_formatter setDateFormat:@" 1:mm"];}
		if (_currenthour == 2) {[_formatter setDateFormat:@" 2:mm"];}
		if (_currenthour == 3) {[_formatter setDateFormat:@" 3:mm"];}
		if (_currenthour == 4) {[_formatter setDateFormat:@" 4:mm"];}
		if (_currenthour == 5) {[_formatter setDateFormat:@" 5:mm"];}
		if (_currenthour == 6) {[_formatter setDateFormat:@" 6:mm"];}
		if (_currenthour == 7) {[_formatter setDateFormat:@" 7:mm"];}
		if (_currenthour == 8) {[_formatter setDateFormat:@" 8:mm"];}
		if (_currenthour == 9) {[_formatter setDateFormat:@" 9:mm"];}
		if (_currenthour == 10) {[_formatter setDateFormat:@"10:mm"];}
		if (_currenthour == 11) {[_formatter setDateFormat:@"11:mm"];}
		if (_currenthour == 12) {[_formatter setDateFormat:@"12:mm"];}
		
		//[self refreshAmpm_imageview];
	} else {
		[_formatter setDateFormat:@"HH:mm"];
	}
	
	NSString* str = [_formatter stringFromDate:date];
	//[_clocknumbers setClockstring:str];
	[view_clockdetail setClockstring:str];
	
	[_formatter setDateFormat:@"ss"];
	str = [_formatter stringFromDate:date];
	[view_clockdetail setSecondstring:str];
	
	
	[view_daydetail setNumericWeekday:_numericweekday];
	[view_datedetail setNumericMonth:_numericmonth];
	
	
	
	[_formatter setDateFormat:@"d"];
	str = [_formatter stringFromDate:date];
	[view_datedetail setDateString:str];
	
	
	[_formatter setDateFormat:@"yyyy"];
	str = [_formatter stringFromDate:date];

	
	[view_datedetail setYearString:str];
	

	[view_alarmdetail setAlarmSound:_currentalarmsound];
	
	
	if (_alarmactive == true) {
		
		if (_userdismissedalert == false) {
			if (_currentalarmhour == _currenthour)
			{
				if (_currentalarmminute == _currentminute) {
					
					if (_alertactive == false) {
						[self activateAlert];
					}
					
				}
			}
		}
		
	}
	
	if (_currentalarmminute != _currentminute) {
		_userdismissedalert = false;
	}
	
	[self refreshAmPm];
	[self RefreshAlarmtime];
	[view_clockdetail refreshDisplay];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code.

}
*/


/////////////////////////////////////////////////////////////////////////
// SOUND PLAYING STUFF
/////////////////////////////////////////////////////////////////////////

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
	_audioplaying = false;
	if (_myaudioplayer != nil)
	{
		NSLog (@"Releasing Audioplayer...");
		[_myaudioplayer release];
	}
}

- (void) configureAudioServices {
	
	AudioSessionInitialize (NULL, NULL,	NULL,NULL);
	
	//UInt32 sessionCategory = kAudioSessionCategory_AmbientSound;
	UInt32 sessionCategory = kAudioSessionCategory_MediaPlayback;
	AudioSessionSetProperty (kAudioSessionProperty_AudioCategory, sizeof (sessionCategory), &sessionCategory);
	
	
	UInt32 audioOverride =  true;
	AudioSessionSetProperty (kAudioSessionProperty_OverrideCategoryMixWithOthers, sizeof (audioOverride), &audioOverride);
	
	AudioSessionSetActive (true);
}

- (void)playClockbuttonsound
{
	[self playSinglesound:@"clockbutton"];
}

- (void)playAlarmbuttonsound
{
	[self playSinglesound:@"clockbuttonhigh"];
}

- (void)playSinglesound:(NSString *)resourcename
{
	[self playSinglesoundEx:resourcename];
}

- (void)playSinglesoundEx:(NSString *)resourcename
{
	if (_audioplaying == true) {
		if (_myaudioplayer != nil)
		{
			[_myaudioplayer stop];
			[_myaudioplayer release];
		}
	}
	
	NSString *buttonPath = [[NSBundle mainBundle] pathForResource:resourcename ofType:@"wav" inDirectory:@"/"];
	
	if (buttonPath == nil) {
		buttonPath = [[NSBundle mainBundle] pathForResource:resourcename ofType:@"WAV" inDirectory:@"/"];
	}
	
	if (buttonPath != nil) {
		CFURLRef sndbuttonURL;
		NSURL *url = [[NSURL alloc] initFileURLWithPath:buttonPath];
		sndbuttonURL = (CFURLRef)url;
		
		_myaudioplayer = [[AVAudioPlayer alloc] initWithContentsOfURL: url error: nil];		
		[url release];
		
		[_myaudioplayer setVolume:1];
		[_myaudioplayer setDelegate:self];
		[_myaudioplayer prepareToPlay];
		[_myaudioplayer play];	
		_audioplaying = true;
	}
}

/////////////////////////////////////////////////////////////////////////
// VOICE ANNOUCEMENT STUFF
/////////////////////////////////////////////////////////////////////////

- (void)announcebuttonPressed
{
	NSLog (@"Announce button pressed.");

	
	if (_timeannouncementplaying == false) {
		_timeannouncementplaying = true;
		[self startAnnouncementTimer];
	}
}

- (int) getsingleNumeric:(int)clocknumeric:(int)digit
{
	if (digit == 1) {
		if (clocknumeric == 0) {return 0;}
		if (clocknumeric == 1) {return 1;}
		if (clocknumeric == 2) {return 2;}
		if (clocknumeric == 3) {return 3;}
		if (clocknumeric == 4) {return 4;}
		if (clocknumeric == 5) {return 5;}
		if (clocknumeric == 6) {return 6;}
		if (clocknumeric == 7) {return 7;}
		if (clocknumeric == 8) {return 8;}
		if (clocknumeric == 9) {return 9;}
		
		if (clocknumeric == 10) {return 1;}
		if (clocknumeric == 11) {return 1;}
		if (clocknumeric == 12) {return 1;}
		if (clocknumeric == 13) {return 1;}
		if (clocknumeric == 14) {return 1;}
		if (clocknumeric == 15) {return 1;}
		if (clocknumeric == 16) {return 1;}
		if (clocknumeric == 17) {return 1;}
		if (clocknumeric == 18) {return 1;}
		if (clocknumeric == 19) {return 1;}
		
		if (clocknumeric == 20) {return 2;}
		if (clocknumeric == 21) {return 2;}
		if (clocknumeric == 22) {return 2;}
		if (clocknumeric == 23) {return 2;}
		if (clocknumeric == 24) {return 2;}
		if (clocknumeric == 25) {return 2;}
		if (clocknumeric == 26) {return 2;}
		if (clocknumeric == 27) {return 2;}
		if (clocknumeric == 28) {return 2;}
		if (clocknumeric == 29) {return 2;}
		
		if (clocknumeric == 30) {return 3;}
		if (clocknumeric == 31) {return 3;}
		if (clocknumeric == 32) {return 3;}
		if (clocknumeric == 33) {return 3;}
		if (clocknumeric == 34) {return 3;}
		if (clocknumeric == 35) {return 3;}
		if (clocknumeric == 36) {return 3;}
		if (clocknumeric == 37) {return 3;}
		if (clocknumeric == 38) {return 3;}
		if (clocknumeric == 39) {return 3;}
		
		if (clocknumeric == 40) {return 4;}
		if (clocknumeric == 41) {return 4;}
		if (clocknumeric == 42) {return 4;}
		if (clocknumeric == 43) {return 4;}
		if (clocknumeric == 44) {return 4;}
		if (clocknumeric == 45) {return 4;}
		if (clocknumeric == 46) {return 4;}
		if (clocknumeric == 47) {return 4;}
		if (clocknumeric == 48) {return 4;}
		if (clocknumeric == 49) {return 4;}
		
		if (clocknumeric == 50) {return 5;}
		if (clocknumeric == 51) {return 5;}
		if (clocknumeric == 52) {return 5;}
		if (clocknumeric == 53) {return 5;}
		if (clocknumeric == 54) {return 5;}
		if (clocknumeric == 55) {return 5;}
		if (clocknumeric == 56) {return 5;}
		if (clocknumeric == 57) {return 5;}
		if (clocknumeric == 58) {return 5;}
		if (clocknumeric == 59) {return 5;}
		
	}
	
	if (digit == 2) {
		if (clocknumeric == 0) {return 0;}
		if (clocknumeric == 1) {return 1;}
		if (clocknumeric == 2) {return 2;}
		if (clocknumeric == 3) {return 3;}
		if (clocknumeric == 4) {return 4;}
		if (clocknumeric == 5) {return 5;}
		if (clocknumeric == 6) {return 6;}
		if (clocknumeric == 7) {return 7;}
		if (clocknumeric == 8) {return 8;}
		if (clocknumeric == 9) {return 9;}
		
		if (clocknumeric == 10) {return 0;}
		if (clocknumeric == 11) {return 1;}
		if (clocknumeric == 12) {return 2;}
		if (clocknumeric == 13) {return 3;}
		if (clocknumeric == 14) {return 4;}
		if (clocknumeric == 15) {return 5;}
		if (clocknumeric == 16) {return 6;}
		if (clocknumeric == 17) {return 7;}
		if (clocknumeric == 18) {return 8;}
		if (clocknumeric == 19) {return 9;}
		
		if (clocknumeric == 20) {return 0;}
		if (clocknumeric == 21) {return 1;}
		if (clocknumeric == 22) {return 2;}
		if (clocknumeric == 23) {return 3;}
		if (clocknumeric == 24) {return 4;}
		if (clocknumeric == 25) {return 5;}
		if (clocknumeric == 26) {return 6;}
		if (clocknumeric == 27) {return 7;}
		if (clocknumeric == 28) {return 8;}
		if (clocknumeric == 29) {return 9;}
		
		if (clocknumeric == 30) {return 0;}
		if (clocknumeric == 31) {return 1;}
		if (clocknumeric == 32) {return 2;}
		if (clocknumeric == 33) {return 3;}
		if (clocknumeric == 34) {return 4;}
		if (clocknumeric == 35) {return 5;}
		if (clocknumeric == 36) {return 6;}
		if (clocknumeric == 37) {return 7;}
		if (clocknumeric == 38) {return 8;}
		if (clocknumeric == 39) {return 9;}
		
		if (clocknumeric == 40) {return 0;}
		if (clocknumeric == 41) {return 1;}
		if (clocknumeric == 42) {return 2;}
		if (clocknumeric == 43) {return 3;}
		if (clocknumeric == 44) {return 4;}
		if (clocknumeric == 45) {return 5;}
		if (clocknumeric == 46) {return 6;}
		if (clocknumeric == 47) {return 7;}
		if (clocknumeric == 48) {return 8;}
		if (clocknumeric == 49) {return 9;}
		
		if (clocknumeric == 50) {return 0;}
		if (clocknumeric == 51) {return 1;}
		if (clocknumeric == 52) {return 2;}
		if (clocknumeric == 53) {return 3;}
		if (clocknumeric == 54) {return 4;}
		if (clocknumeric == 55) {return 5;}
		if (clocknumeric == 56) {return 6;}
		if (clocknumeric == 57) {return 7;}
		if (clocknumeric == 58) {return 8;}
		if (clocknumeric == 59) {return 9;}	
	}
	
	return 0;
}

- (void)play12Hournumeric:(int)numeric
{
	if (numeric == 0) {[self playSinglesound:@"12"];}
	if (numeric == 1) {[self playSinglesound:@"1"];}
	if (numeric == 2) {[self playSinglesound:@"2"];}
	if (numeric == 3) {[self playSinglesound:@"3"];}
	if (numeric == 4) {[self playSinglesound:@"4"];}
	if (numeric == 5) {[self playSinglesound:@"5"];}
	if (numeric == 6) {[self playSinglesound:@"6"];}
	if (numeric == 7) {[self playSinglesound:@"7"];}
	if (numeric == 8) {[self playSinglesound:@"8"];}
	if (numeric == 9) {[self playSinglesound:@"9"];}
	if (numeric == 10) {[self playSinglesound:@"10"];}
	
	if (numeric == 11) {[self playSinglesound:@"11"];}
	if (numeric == 12) {[self playSinglesound:@"12"];}
	if (numeric == 13) {[self playSinglesound:@"1"];}
	if (numeric == 14) {[self playSinglesound:@"2"];}
	if (numeric == 15) {[self playSinglesound:@"3"];}
	if (numeric == 16) {[self playSinglesound:@"4"];}
	if (numeric == 17) {[self playSinglesound:@"5"];}
	if (numeric == 18) {[self playSinglesound:@"6"];}
	if (numeric == 19) {[self playSinglesound:@"7"];}
	if (numeric == 20) {[self playSinglesound:@"8"];}
	
	if (numeric == 21) {[self playSinglesound:@"9"];}
	if (numeric == 22) {[self playSinglesound:@"10"];}
	if (numeric == 23) {[self playSinglesound:@"11"];}
}

- (void)playKlingonnumeral:(int)numeric
{
	bool klingon = true;
	
	if (klingon == true) {
		if (numeric == 0) {[self playSinglesound:@"0kl"];}
		if (numeric == 1) {[self playSinglesound:@"1kl"];}
		if (numeric == 2) {[self playSinglesound:@"2kl"];}
		if (numeric == 3) {[self playSinglesound:@"3kl"];}
		if (numeric == 4) {[self playSinglesound:@"4kl"];}
		if (numeric == 5) {[self playSinglesound:@"5kl"];}
		if (numeric == 6) {[self playSinglesound:@"6kl"];}
		if (numeric == 7) {[self playSinglesound:@"7kl"];}
		if (numeric == 8) {[self playSinglesound:@"8kl"];}
		if (numeric == 9) {[self playSinglesound:@"9kl"];}
	} else {
		if (numeric == 0) {[self playSinglesound:@"0"];}
		if (numeric == 1) {[self playSinglesound:@"1"];}
		if (numeric == 2) {[self playSinglesound:@"2"];}
		if (numeric == 3) {[self playSinglesound:@"3"];}
		if (numeric == 4) {[self playSinglesound:@"4"];}
		if (numeric == 5) {[self playSinglesound:@"5"];}
		if (numeric == 6) {[self playSinglesound:@"6"];}
		if (numeric == 7) {[self playSinglesound:@"7"];}
		if (numeric == 8) {[self playSinglesound:@"8"];}
		if (numeric == 9) {[self playSinglesound:@"9"];}
	}
}

- (void)playNumericsound:(int)numeric
{
	if (numeric == 0) {[self playSinglesound:@"0"];}
	if (numeric == 1) {[self playSinglesound:@"01"];}
	if (numeric == 2) {[self playSinglesound:@"02"];}
	if (numeric == 3) {[self playSinglesound:@"03"];}
	if (numeric == 4) {[self playSinglesound:@"04"];}
	if (numeric == 5) {[self playSinglesound:@"05"];}
	if (numeric == 6) {[self playSinglesound:@"06"];}
	if (numeric == 7) {[self playSinglesound:@"07"];}
	if (numeric == 8) {[self playSinglesound:@"08"];}
	if (numeric == 9) {[self playSinglesound:@"09"];}
	if (numeric == 10) {[self playSinglesound:@"10"];}
	
	if (numeric == 11) {[self playSinglesound:@"11"];}
	if (numeric == 12) {[self playSinglesound:@"12"];}
	if (numeric == 13) {[self playSinglesound:@"13"];}
	if (numeric == 14) {[self playSinglesound:@"14"];}
	if (numeric == 15) {[self playSinglesound:@"15"];}
	if (numeric == 16) {[self playSinglesound:@"16"];}
	if (numeric == 17) {[self playSinglesound:@"17"];}
	if (numeric == 18) {[self playSinglesound:@"18"];}
	if (numeric == 19) {[self playSinglesound:@"19"];}
	if (numeric == 20) {[self playSinglesound:@"20"];}
	
	if (numeric == 21) {[self playSinglesound:@"21"];}
	if (numeric == 22) {[self playSinglesound:@"22"];}
	if (numeric == 23) {[self playSinglesound:@"23"];}
	if (numeric == 24) {[self playSinglesound:@"24"];}
	if (numeric == 25) {[self playSinglesound:@"25"];}
	if (numeric == 26) {[self playSinglesound:@"26"];}
	if (numeric == 27) {[self playSinglesound:@"27"];}
	if (numeric == 28) {[self playSinglesound:@"28"];}
	if (numeric == 29) {[self playSinglesound:@"29"];}
	if (numeric == 30) {[self playSinglesound:@"30"];}
	
	if (numeric == 31) {[self playSinglesound:@"31"];}
	if (numeric == 32) {[self playSinglesound:@"32"];}
	if (numeric == 33) {[self playSinglesound:@"33"];}
	if (numeric == 34) {[self playSinglesound:@"34"];}
	if (numeric == 35) {[self playSinglesound:@"35"];}
	if (numeric == 36) {[self playSinglesound:@"36"];}
	if (numeric == 37) {[self playSinglesound:@"37"];}
	if (numeric == 38) {[self playSinglesound:@"38"];}
	if (numeric == 39) {[self playSinglesound:@"39"];}
	if (numeric == 40) {[self playSinglesound:@"40"];}
	
	if (numeric == 41) {[self playSinglesound:@"41"];}
	if (numeric == 42) {[self playSinglesound:@"42"];}
	if (numeric == 43) {[self playSinglesound:@"43"];}
	if (numeric == 44) {[self playSinglesound:@"44"];}
	if (numeric == 45) {[self playSinglesound:@"45"];}
	if (numeric == 46) {[self playSinglesound:@"46"];}
	if (numeric == 47) {[self playSinglesound:@"47"];}
	if (numeric == 48) {[self playSinglesound:@"48"];}
	if (numeric == 49) {[self playSinglesound:@"49"];}
	if (numeric == 50) {[self playSinglesound:@"50"];}
	
	if (numeric == 51) {[self playSinglesound:@"51"];}
	if (numeric == 52) {[self playSinglesound:@"52"];}
	if (numeric == 53) {[self playSinglesound:@"53"];}
	if (numeric == 54) {[self playSinglesound:@"54"];}
	if (numeric == 55) {[self playSinglesound:@"55"];}
	if (numeric == 56) {[self playSinglesound:@"56"];}
	if (numeric == 57) {[self playSinglesound:@"57"];}
	if (numeric == 58) {[self playSinglesound:@"58"];}
	if (numeric == 59) {[self playSinglesound:@"59"];}
	
}

- (void)triggerAnnounce:(id)sender
{
	int num = 0;
	int wantedhour = 0;
	
	if (_klingonmode == false) {
	
		if (_iannouncenumber == 0) {
			[self playSinglesound:@"Time"];
		}
	
		if (_iannouncenumber == 1) {
			if (_12hourmode == true) {
				[self play12Hournumeric:_currenthour];
			} else {
				[self playNumericsound:_currenthour];
			}
		
		}
	
		if (_iannouncenumber == 2) {
			if (_currentminute == 0) {
				if (_12hourmode == true) {
					[self playSinglesound:@"oclock"];
				} else {
					[self playSinglesound:@"hundred"];
				}
			} else {
				[self playNumericsound:_currentminute];
			}
		
		}
	
		if (_12hourmode == true) {
			if (_iannouncenumber == 3) {
				if (_ispm == true) {
					[self playSinglesound:@"pm"];
				} else {
					[self playSinglesound:@"am"];
				}
			}
		}
	
		if (_iannouncenumber >= 4) {
			[announceTimer invalidate];
			_timeannouncementplaying = false;
		}
	} else {
		// The thing to note about speaking in klingon mode is that we speak individual numerals only
		// so if the time is 12:34, we say one, two, three, four - we can't say twelve, thirtyfour.
		// The reason is because there is limited material on the net about pronouncing numbers in klingon.
		if (_12hourmode == true) {
			wantedhour = _currenthour12;
		} else {
			wantedhour = _currenthour;
		}
		
		if (wantedhour >= 10) { // Each announcement trigger should announce the current numeral.
		
			if (_iannouncenumber == 0) {
				num = [self getsingleNumeric:wantedhour:1];
				[self playKlingonnumeral:num];
			}
			
			if (_iannouncenumber == 1) {
				num = [self getsingleNumeric:wantedhour:2];
				[self playKlingonnumeral:num];			
			}
			
			if (_currentminute >= 10) {
				if (_iannouncenumber == 2) {
					num = [self getsingleNumeric:_currentminute:1];
					[self playKlingonnumeral:num];
				}
				
				if (_iannouncenumber == 3) {
					num = [self getsingleNumeric:_currentminute:2];
					[self playKlingonnumeral:num];
				}
			} else {
				if (_iannouncenumber == 2) {
					[self playKlingonnumeral:0];
				}
				
				if (_iannouncenumber == 3) {
					num = [self getsingleNumeric:_currentminute:1];
					[self playKlingonnumeral:num];
				}
			}
			
			if (_currentsecond >= 10) {
				if (_iannouncenumber == 4) {
					num = [self getsingleNumeric:_currentsecond:1];
					[self playKlingonnumeral:num];
				}
				
				if (_iannouncenumber == 5) {
					num = [self getsingleNumeric:_currentsecond:2];
					[self playKlingonnumeral:num];
				}
			} else {
				if (_iannouncenumber == 4) {
					[self playKlingonnumeral:0];
				}
				
				if (_iannouncenumber == 5) {
					num = [self getsingleNumeric:_currentsecond:1];
					[self playKlingonnumeral:num];
				}
			}
			
			
			
		} else { // The hour is less than 10. - i.e. we only have a single numeral for the hour, not 2.
			
			if (_iannouncenumber == 0) {
				num = [self getsingleNumeric:wantedhour:1];
				[self playKlingonnumeral:num];
			}
			
			if (_currentminute >= 10) {
				if (_iannouncenumber == 1) {
					num = [self getsingleNumeric:_currentminute:1];
					[self playKlingonnumeral:num];			
				}
				
				
				if (_iannouncenumber == 2) {
					num = [self getsingleNumeric:_currentminute:2];
					[self playKlingonnumeral:num];
				}
			} else {
				if (_iannouncenumber == 1) {
					[self playKlingonnumeral:0];			
				}
								
				if (_iannouncenumber == 2) {
					num = [self getsingleNumeric:_currentminute:1];
					[self playKlingonnumeral:num];
				}
			}
			
			
			if (_currentsecond >= 10) {
				if (_iannouncenumber == 3) {
					num = [self getsingleNumeric:_currentsecond:1];
					[self playKlingonnumeral:num];
				}
				
				if (_iannouncenumber == 4) {
					num = [self getsingleNumeric:_currentsecond:2];
					[self playKlingonnumeral:num];
				}
			} else {
				if (_iannouncenumber == 3) {
					[self playKlingonnumeral:0];
				}
				
				if (_iannouncenumber == 4) {
					num = [self getsingleNumeric:_currentsecond:1];
					[self playKlingonnumeral:num];
				}
			}
			
			
		}
		
		if (_iannouncenumber >= 6) {
			[announceTimer invalidate];
			_timeannouncementplaying = false;
		}
		
	}
	
	_iannouncenumber++;
}


- (void)startAnnouncementTimer
{
	_iannouncenumber = 0;
	if (_klingonmode == false) {
		announceTimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)1.2 target:self selector:@selector(triggerAnnounce:) userInfo:nil repeats:TRUE];
	} else {
		announceTimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)0.6 target:self selector:@selector(triggerAnnounce:) userInfo:nil repeats:TRUE];
	}
	[announceTimer retain];
}

/////////////////////////////////////////////////////////////////////////
// FLASHING ALERT STUFF AND SNOOZE - When the alarm is going off
/////////////////////////////////////////////////////////////////////////

- (void)activateAlert
{
	_alertactive = true;
	view_daydetail.hidden = true;
	view_datedetail.hidden = true;
	
	btn_alarmdismiss.hidden = false;
	btn_alarmsnooze.hidden = false;
	imgview_alert.hidden = false;
	
	alertTrigger = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)1 target:self selector:@selector(alertTrigger:) userInfo:nil repeats:TRUE];
	[alertTrigger retain];
}

- (void)activateSnooze
{
	snoozeTrigger = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)600 target:self selector:@selector(snoozeTrigger:) userInfo:nil repeats:FALSE];
	[snoozeTrigger retain];
}

- (void)deactivateAlert
{
	_userdismissedalert = true;
	_alertactive = false;
	if (alertTrigger != nil) {
		[alertTrigger invalidate];
		alertTrigger = nil;
	}
	
	view_daydetail.hidden = false;
	view_datedetail.hidden = false;
	
	btn_alarmdismiss.hidden = true;
	btn_alarmsnooze.hidden = true;
	imgview_alert.hidden = true;	
}

- (void)snoozeTrigger:(id)sender
{
	[self activateAlert];
}

- (void)alertTrigger:(id)sender
{
	if (imgview_alert.hidden == true) {
		imgview_alert.hidden = false;
		
		// Play the sound here too
		[self playCurrentalarm];
	} else {
		imgview_alert.hidden = true;
	}
}

- (void)playCurrentalarm
{
	if (_currentalarmsound == 1) {[self playSinglesound:@"alarmsound1"];}
	if (_currentalarmsound == 2) {[self playSinglesound:@"alarmsound2"];}
	if (_currentalarmsound == 3) {[self playSinglesound:@"alarmsound3"];}
	if (_currentalarmsound == 4) {[self playSinglesound:@"alarmsound4"];}
	if (_currentalarmsound == 5) {[self playSinglesound:@"alarmsound5"];}
	if (_currentalarmsound == 6) {[self playSinglesound:@"alarmsound6"];}
	if (_currentalarmsound == 7) {[self playSinglesound:@"alarmsound7"];}
	if (_currentalarmsound == 8) {[self playSinglesound:@"alarmsound8"];}
	if (_currentalarmsound == 9) {[self playSinglesound:@"alarmsound9"];}
	if (_currentalarmsound == 10) {[self playSinglesound:@"alarmsound10"];}	
}


/////////////////////////////////////////////////////////////////////////
// LOCAL NOTIFICATION STUFF
/////////////////////////////////////////////////////////////////////////

- (void)scheduleGenericalarm
{
	if (_alarmactive == false) {
		return;
	}
	
	NSDate *now = [NSDate date];
	
	NSCalendar *gregorian = [[NSCalendar alloc]
							 initWithCalendarIdentifier:NSGregorianCalendar];
	
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit;
	
	
	NSDateComponents *alarm_components = [gregorian components:unitFlags fromDate:now];
	
	//int alarmday = [alarm_components day];
	//int alarmmonth = [alarm_components month];
	//int alarmyear = [alarm_components year];
	//int alarmhour = _currentalarmhour;
	//int alarmminute = _currentalarmminute;
	
	[alarm_components setHour:_currentalarmhour];
	[alarm_components setMinute:_currentalarmminute];
	
	//NSLog (@"Alarm Day: %i", alarmday);
	//NSLog (@"Alarm Month: %i", alarmmonth);
	//NSLog (@"Alarm Year: %i", alarmyear);
	//NSLog (@"Alarm Hour: %i", alarmhour);
	//NSLog (@"Alarm Minute: %i", alarmminute);
	
	
	NSDate *alarm_today = [gregorian dateFromComponents:alarm_components];
	
	//NSLog (@"Alarm Date is: %@", alarm_today);
	
	NSDateComponents *addcomp = [[NSDateComponents alloc] init];
	addcomp.day = 1;
	
	NSDate *alarm_tomorrow = [gregorian dateByAddingComponents:addcomp toDate:alarm_today options:0];
	
	//NSLog (@"Alarm Tomorrow: %@", alarm_tomorrow);
	
	if ([alarm_today timeIntervalSinceDate:now] >= 0) {
		//NSLog (@"Alarm today is in the future");
		
		[self scheduleAlarmForDate:alarm_today];
	} else {
		//NSLog (@"Alarm today is in the past");
		
		[self scheduleAlarmForDate:alarm_tomorrow];
	}
	
	//NSDate *now = [NSDate date];
	//NSDate *future = [now dateByAddingTimeInterval:10];
	
	
	
	[gregorian release];
	[addcomp release];
	//[self scheduleAlarmForDate:future];
}

- (void)clearAllLocalnotifications
{
	UIApplication *app = [UIApplication sharedApplication];
	NSArray *oldNotifications = [app scheduledLocalNotifications];
	
	// Clear out the old notifications before scheduling a new one
	if ([oldNotifications count] > 0) {
		[app cancelAllLocalNotifications];
	}
}

- (void)scheduleAlarmForDate:(NSDate*)theDate
{
	UIApplication *app = [UIApplication sharedApplication];
	NSArray *oldNotifications = [app scheduledLocalNotifications];
	
	// Clear out the old notifications before scheduling a new one
	if ([oldNotifications count] > 0) {
		[app cancelAllLocalNotifications];
	}
	
	// Create a new notification
	UILocalNotification* alarm = [[[UILocalNotification alloc] init] autorelease];
	
	if (alarm)
	{
		alarm.fireDate = theDate;
		alarm.timeZone = [NSTimeZone defaultTimeZone];
		alarm.repeatInterval = NSDayCalendarUnit;
		
		//if (_usingsound1 == true) {
		//alarm.soundName = @"clocksound1repeat.wav";
		//} else {
		alarm.soundName = @"alarmsound7-repeat.wav";
		//}
		
		alarm.alertBody = @"Alarm";
		
		[app scheduleLocalNotification:alarm];
	}
}

- (void)dealloc {
    [super dealloc];
}


@end
