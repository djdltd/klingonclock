//
//  ClockNumbers.h
//  LCARS Clock
//
//  Created by Danny Draper on 27/06/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ClockNumbers : NSObject {
	NSMutableArray *_clocknumbers;
	int _curframe;
	NSMutableString *_curclockstring;
	int _dmin;
	int _dhour;
	UIImage *frame;
	UIImage *tempframe;
	UniChar singlechar;
	
	CGFloat _charwidth;
	CGFloat _charheight;
	CGFloat _colonwidth;
	CGFloat _xposition;
	CGFloat _yposition;
	
	CGFloat _startx;
	CGFloat _starty;	
	
	
	bool _usingcolon;
	
}

- (void) Initialise:(CGFloat) startxpos:(CGFloat) startypos: (CGFloat) charwidth: (CGFloat) charheight: (bool) usingcolon: (CGFloat) colonwidth: (NSString *) resourcename;
- (void) setClockstring:(NSString *)clockstring;
- (void) PaintString;
- (void) setPosition:(CGFloat)xpos :(CGFloat)ypos;
- (void) setnewResource:(NSString *)resourcename;

@end
