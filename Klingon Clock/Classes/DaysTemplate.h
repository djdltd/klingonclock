//
//  DaysTemplate.h
//  LCARS Clock
//
//  Created by Danny Draper on 27/06/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DaysTemplate : NSObject {

	NSMutableArray *_clocknumbers;
	int _curframe;
	int _dmin;
	int _dhour;
	int _xloc;
	int _yloc;
	UIImage *frame;
	int _iweekday;
}

- (void) Initialise;
- (void) PaintString;
- (void) setLocation:(CGFloat) xloc:(CGFloat) yloc;
- (void) setNumericWeekday:(int)weekday;

@end
