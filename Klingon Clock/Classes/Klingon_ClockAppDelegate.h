//
//  Klingon_ClockAppDelegate.h
//  Klingon Clock
//
//  Created by Danny Draper on 20/12/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Klingon_ClockViewController;

@interface Klingon_ClockAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    Klingon_ClockViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet Klingon_ClockViewController *viewController;

@end

