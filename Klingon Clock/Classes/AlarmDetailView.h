//
//  AlarmDetailView.h
//  Klingon Clock
//
//  Created by Danny Draper on 01/01/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClockNumbers.h"

@interface AlarmDetailView : UIView {

	bool _landscape;
	bool _klingonmode;
	
	ClockNumbers *_alarm;
	ClockNumbers *_sound;
	
}

- (void)setLandscapemode:(bool)landscape;
- (void)setKlingonmode:(bool)klingonmode;
- (void)setAlarmString:(NSString *)alarmstring;
- (void)setAlarmSound:(int)alarmsound;

@end
