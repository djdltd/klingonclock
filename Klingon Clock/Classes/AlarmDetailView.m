//
//  AlarmDetailView.m
//  Klingon Clock
//
//  Created by Danny Draper on 01/01/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import "AlarmDetailView.h"


@implementation AlarmDetailView

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super initWithCoder:decoder])
    {
		_landscape = false;
		_klingonmode = false;
		
		self.backgroundColor = [UIColor clearColor];
		
		[_alarm = [[ClockNumbers alloc] init] retain];
		[_alarm Initialise:2.0f:3.0f:31.0f:42.0f:true:8.0f:@"AlarmNumbers.png"];
		[_alarm setPosition:30.0f:5.0f];
		
		[_sound = [[ClockNumbers alloc] init] retain];
		[_sound Initialise:2.0f:3.0f:31.0f:42.0f:true:8.0f:@"AlarmNumbers.png"];
		[_sound setPosition:223.0f:5.0f];
		
		[self setNeedsDisplay];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
    }
    return self;
}

- (void)setLandscapemode:(bool)landscape
{
	_landscape = landscape;
	
	[self setNeedsDisplay];
}


- (void)setKlingonmode:(bool)klingonmode
{
	_klingonmode = klingonmode;
	
	if (klingonmode == true) {
		[_alarm setnewResource:@"AlarmNumbersKlingon.png"];
		[_sound setnewResource:@"AlarmNumbersKlingon.png"];
	} else {
		[_alarm setnewResource:@"AlarmNumbers.png"];
		[_sound setnewResource:@"AlarmNumbers.png"];
	}
	
	[self setNeedsDisplay];
}


- (void)setAlarmString:(NSString *)alarmstring
{
	[_alarm setClockstring:alarmstring];
	
	[self setNeedsDisplay];
}

- (void)setAlarmSound:(int)alarmsound
{
	NSNumber *numsound;
	numsound = [[NSNumber alloc] initWithInt:alarmsound];
	[_sound setClockstring:[numsound stringValue]];
	
	[self setNeedsDisplay];
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
	[_alarm PaintString];
	[_sound PaintString];
}


- (void)dealloc {
    [super dealloc];
}


@end
