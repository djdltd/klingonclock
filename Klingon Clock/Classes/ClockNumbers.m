//
//  ClockNumbers.m
//  LCARS Clock
//
//  Created by Danny Draper on 27/06/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import "ClockNumbers.h"


@implementation ClockNumbers

- (void) Initialise:(CGFloat) startxpos:(CGFloat) startypos: (CGFloat) charwidth: (CGFloat) charheight: (bool) usingcolon: (CGFloat) colonwidth: (NSString *) resourcename
{
	
	NSLog (@"Clock Numbers Initialise has been called.");
	
	_curframe = 0;
	_dmin = 0;
	_dhour = 0;
	
	CGFloat startx = startxpos;
	CGFloat starty = startypos;
	
	CGFloat yoffset = 0.0f;
	CGFloat xoffset = 0.0f;
	
	_startx = startx;
	_starty = starty;
	
	
	_charwidth = charwidth;
	_charheight = charheight;
	_colonwidth = colonwidth;
	_usingcolon = usingcolon;
	
	//CGPoint currentFramelocation;
	
	_clocknumbers = [NSMutableArray new];
	_curclockstring = [NSMutableString new];
	
	UIImage *clocknumbers;
	
	clocknumbers = [UIImage imageNamed:resourcename];
	
	bool bUsingstandardwidth = true;
	
	CGRect imageRect;


	int i = 0;
	
	int maxchars = 11;
	
	if (usingcolon == true) {
		maxchars = 11;
	} else {
		maxchars = 10;
	}
	
	
	// First level of numbers
	for (i=0;i<maxchars;++i)
	{
		bUsingstandardwidth = true;
		imageRect.origin = CGPointMake (startx + xoffset, starty + yoffset);
			
		
		if (usingcolon == true) {
			if (i == 10) {
				imageRect.size = CGSizeMake(colonwidth, _charheight);
				xoffset+= _colonwidth;
				bUsingstandardwidth = false;
			}
		}
		
		
		if (bUsingstandardwidth == true) {
			imageRect.size = CGSizeMake(_charwidth, _charheight);
			xoffset+= _charwidth;
		}
		
		
		tempframe = [UIImage imageWithCGImage:CGImageCreateWithImageInRect([clocknumbers CGImage], imageRect)];
		[_clocknumbers addObject:tempframe];		
	}
	
}

- (void) setnewResource:(NSString *)resourcename
{
	NSLog (@"Clock Numbers setNewResource has been called.");
	
	[_clocknumbers removeAllObjects];
	
	CGFloat xoffset = 0.0f;
	UIImage *clocknumbers;
	
	clocknumbers = [UIImage imageNamed:resourcename];
	
	bool bUsingstandardwidth = true;
	
	CGRect imageRect;
	
	int i = 0;
	
	int maxchars = 11;
	
	if (_usingcolon == true) {
		maxchars = 11;
	} else {
		maxchars = 10;
	}
	
	
	// First level of numbers
	for (i=0;i<maxchars;++i)
	{
		bUsingstandardwidth = true;
		imageRect.origin = CGPointMake (_startx + xoffset, _starty);
		
		
		if (_usingcolon == true) {
			if (i == 10) {
				imageRect.size = CGSizeMake(_colonwidth, _charheight);
				xoffset+= _colonwidth;
				bUsingstandardwidth = false;
			}
		}
		
		
		if (bUsingstandardwidth == true) {
			imageRect.size = CGSizeMake(_charwidth, _charheight);
			xoffset+= _charwidth;
		}
		
		
		tempframe = [UIImage imageWithCGImage:CGImageCreateWithImageInRect([clocknumbers CGImage], imageRect)];
		[_clocknumbers addObject:tempframe];		
	}
	
}

- (void) setClockstring:(NSString *)clockstring
{
	[_curclockstring setString:clockstring];

}

- (void) setPosition:(CGFloat)xpos :(CGFloat)ypos
{
	_xposition = xpos;
	_yposition = ypos;
}

- (void) PaintString
{
	int c = 0;
	
	CGFloat xoffset = 0.0f;
	CGFloat xincrement = _charwidth;
	
	CGFloat xlocation = _xposition;
	CGFloat ylocation = _yposition;
	
	for (c=0;c<[_curclockstring length];++c)
	{
		singlechar = [_curclockstring characterAtIndex:c];
		
		
		if (singlechar == '0') {
			frame = [_clocknumbers objectAtIndex:0];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '1') {
			frame = [_clocknumbers objectAtIndex:1];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '2') {
			frame = [_clocknumbers objectAtIndex:2];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '3') {
			frame = [_clocknumbers objectAtIndex:3];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '4') {
			frame = [_clocknumbers objectAtIndex:4];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '5') {
			frame = [_clocknumbers objectAtIndex:5];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '6') {
			frame = [_clocknumbers objectAtIndex:6];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '7') {
			frame = [_clocknumbers objectAtIndex:7];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '8') {
			frame = [_clocknumbers objectAtIndex:8];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '9') {
			frame = [_clocknumbers objectAtIndex:9];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == ':') {
			frame = [_clocknumbers objectAtIndex:10];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=_colonwidth;
		}
		
		if (singlechar == ' ') {
			xoffset+=(xincrement/2);			
		}
		
	}
}

@end
