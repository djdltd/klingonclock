//
//  DaysTemplate.m
//  LCARS Clock
//
//  Created by Danny Draper on 27/06/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import "DaysTemplate.h"


@implementation DaysTemplate


- (void) Initialise
{
	_curframe = 0;
	_dmin = 0;
	_dhour = 0;
	
	CGFloat startx = 9.0f;
	CGFloat starty = 11.0f;
	
	CGFloat yoffset = 0.0f;
	CGFloat xoffset = 0.0f;
	
	//CGPoint currentFramelocation;
	
	_clocknumbers = [NSMutableArray new];
	
	UIImage *clocknumbers;
	
	clocknumbers = [UIImage imageNamed:@"Days.png"];
	
	
	CGRect imageRect;
	UIImage *frame1;
	
	int i = 0;
	_xloc = 10.0f;
	_yloc = 5.0f;
	
	// First level of numbers
	for (i=0;i<7;++i)
	{
		imageRect.origin = CGPointMake (startx + xoffset, starty + yoffset);
		imageRect.size = CGSizeMake(348.0f, 44.0f);	
		frame1 = [UIImage imageWithCGImage:CGImageCreateWithImageInRect([clocknumbers CGImage], imageRect)];
		
		[_clocknumbers addObject:frame1];
		
		//xoffset+=25.0f;
		yoffset += 44.0f;
	}
}

- (void) setLocation:(CGFloat) xloc:(CGFloat) yloc
{
	_xloc = xloc;
	_yloc = yloc;
}

- (void) setNumericWeekday:(int)weekday
{
	_iweekday = weekday;
}

- (void) PaintString
{
	
	CGFloat xoffset = 0.0f;
	CGFloat xlocation = _xloc;
	CGFloat ylocation = _yloc;
	
	
	/*
	if ([_curclockstring rangeOfString:@"Saturday"].location != NSNotFound) {
		frame = [_clocknumbers objectAtIndex:0];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if ([_curclockstring rangeOfString:@"Sunday"].location != NSNotFound) {
		frame = [_clocknumbers objectAtIndex:1];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if ([_curclockstring rangeOfString:@"Monday"].location != NSNotFound) {
		frame = [_clocknumbers objectAtIndex:2];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if ([_curclockstring rangeOfString:@"Tuesday"].location != NSNotFound) {
		frame = [_clocknumbers objectAtIndex:3];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if ([_curclockstring rangeOfString:@"Wednesday"].location != NSNotFound) {
		frame = [_clocknumbers objectAtIndex:4];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if ([_curclockstring rangeOfString:@"Thursday"].location != NSNotFound) {
		frame = [_clocknumbers objectAtIndex:5];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if ([_curclockstring rangeOfString:@"Friday"].location != NSNotFound) {
		frame = [_clocknumbers objectAtIndex:6];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	*/
	
	if (_iweekday == 1) { // Sunday
		frame = [_clocknumbers objectAtIndex:6];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if (_iweekday == 2) { // Monday
		frame = [_clocknumbers objectAtIndex:0];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if (_iweekday == 3) { // Tuesday
		frame = [_clocknumbers objectAtIndex:1];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if (_iweekday == 4) { // Wednesday
		frame = [_clocknumbers objectAtIndex:2];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if (_iweekday == 5) { // Thursday
		frame = [_clocknumbers objectAtIndex:3];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if (_iweekday == 6) { // Friday
		frame = [_clocknumbers objectAtIndex:4];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if (_iweekday == 7) { // Saturday
		frame = [_clocknumbers objectAtIndex:5];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
}

@end
