//
//  Klingon_ClockViewController.h
//  Klingon Clock
//
//  Created by Danny Draper on 20/12/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClockView.h"

@interface Klingon_ClockViewController : UIViewController {
	IBOutlet ClockView *clockview;
}

@property (retain, nonatomic) ClockView *clockview;

- (void)saveSettings;

@end

