//
//  MonthsTemplate.m
//  LCARS Clock
//
//  Created by Danny Draper on 27/06/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import "MonthsTemplate.h"


@implementation MonthsTemplate


- (void) Initialise
{
	_curframe = 0;
	_dmin = 0;
	_dhour = 0;
	
	CGFloat startx = 10.0f;
	CGFloat starty = 12.0f;
	
	CGFloat yoffset = 0.0f;
	CGFloat xoffset = 0.0f;
	
	//CGPoint currentFramelocation;
	
	_clocknumbers = [NSMutableArray new];
	
	UIImage *clocknumbers;
	
	clocknumbers = [UIImage imageNamed:@"Months.png"];
	
	
	CGRect imageRect;
	UIImage *frame1;
	
	int i = 0;
	_xloc = 10.0f;
	_yloc = 10.0f;
	
	// First level of numbers
	for (i=0;i<12;++i)
	{
		imageRect.origin = CGPointMake (startx + xoffset, starty + yoffset);
		imageRect.size = CGSizeMake(327.0f, 42.0f);	
		frame1 = [UIImage imageWithCGImage:CGImageCreateWithImageInRect([clocknumbers CGImage], imageRect)];
		
		[_clocknumbers addObject:frame1];
		
		//xoffset+=25.0f;
		yoffset += 42.0f;
	}
}

- (void) setLocation:(CGFloat) xloc:(CGFloat) yloc
{
	_xloc = xloc;
	_yloc = yloc;
}

- (void) setNumericMonth:(int)month
{
	_imonth = month;
}

- (void) PaintString
{
	
	CGFloat xoffset = 0.0f;
	CGFloat xlocation = _xloc;
	CGFloat ylocation = _yloc;
	
	/*
	if ([_curclockstring rangeOfString:@"January"].location != NSNotFound) {
		frame = [_clocknumbers objectAtIndex:0];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if ([_curclockstring rangeOfString:@"February"].location != NSNotFound) {
		frame = [_clocknumbers objectAtIndex:1];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if ([_curclockstring rangeOfString:@"March"].location != NSNotFound) {
		frame = [_clocknumbers objectAtIndex:2];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if ([_curclockstring rangeOfString:@"April"].location != NSNotFound) {
		frame = [_clocknumbers objectAtIndex:3];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if ([_curclockstring rangeOfString:@"May"].location != NSNotFound) {
		frame = [_clocknumbers objectAtIndex:4];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if ([_curclockstring rangeOfString:@"June"].location != NSNotFound) {
		frame = [_clocknumbers objectAtIndex:5];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if ([_curclockstring rangeOfString:@"July"].location != NSNotFound) {
		frame = [_clocknumbers objectAtIndex:6];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if ([_curclockstring rangeOfString:@"August"].location != NSNotFound) {
		frame = [_clocknumbers objectAtIndex:7];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if ([_curclockstring rangeOfString:@"September"].location != NSNotFound) {
		frame = [_clocknumbers objectAtIndex:8];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if ([_curclockstring rangeOfString:@"October"].location != NSNotFound) {
		frame = [_clocknumbers objectAtIndex:9];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if ([_curclockstring rangeOfString:@"November"].location != NSNotFound) {
		frame = [_clocknumbers objectAtIndex:10];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if ([_curclockstring rangeOfString:@"December"].location != NSNotFound) {
		frame = [_clocknumbers objectAtIndex:11];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	*/
	
	if (_imonth == 1)
	{
		frame = [_clocknumbers objectAtIndex:0];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if (_imonth == 2)
	{
		frame = [_clocknumbers objectAtIndex:1];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if (_imonth == 3)
	{
		frame = [_clocknumbers objectAtIndex:2];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if (_imonth == 4)
	{
		frame = [_clocknumbers objectAtIndex:3];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if (_imonth == 5)
	{
		frame = [_clocknumbers objectAtIndex:4];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if (_imonth == 6)
	{
		frame = [_clocknumbers objectAtIndex:5];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if (_imonth == 7)
	{
		frame = [_clocknumbers objectAtIndex:6];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if (_imonth == 8)
	{
		frame = [_clocknumbers objectAtIndex:7];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if (_imonth == 9)
	{
		frame = [_clocknumbers objectAtIndex:8];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if (_imonth == 10)
	{
		frame = [_clocknumbers objectAtIndex:9];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if (_imonth == 11)
	{
		frame = [_clocknumbers objectAtIndex:10];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if (_imonth == 12)
	{
		frame = [_clocknumbers objectAtIndex:11];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
}


@end
