//
//  DateDetailView.m
//  Klingon Clock
//
//  Created by Danny Draper on 31/12/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import "DateDetailView.h"


@implementation DateDetailView

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super initWithCoder:decoder])
    {
		_landscape = false;
		_klingonmode = false;
		
		self.backgroundColor = [UIColor clearColor];
		_numericmonth = 0;
		
		[_months = [[MonthsTemplate alloc] init] retain];
		[_months Initialise];
		[_months setLocation:200.0f:5.0f];
		
		[_date = [[ClockNumbers alloc] init] retain];
		[_date Initialise:7.0f:4.0f:39.0f:46.0f:false:0.0f:@"DateNumbers.png"];

		[_year = [[ClockNumbers alloc] init] retain];
		[_year Initialise:7.0f:4.0f:39.0f:46.0f:false:0.0f:@"DateNumbers.png"];
		
		[_klingonmonth = [[ClockNumbers alloc] init] retain];
		[_klingonmonth Initialise:7.0f:4.0f:39.0f:46.0f:false:0.0f:@"DateNumbersKlingon.png"];
		[_klingonmonth setPosition:250.0f:5.0f];
		
		[self setNeedsDisplay];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
    }
    return self;
}

- (void)setLandscapemode:(bool)landscape
{
	_landscape = landscape;
	if (landscape == true) {
		
	} else {
		
	}
	
	[self repositionMonth];
	[self setNeedsDisplay];
}

- (void)repositionMonth
{
	if (_landscape == true) {
		if (_numericmonth == 1) {[_months setLocation:220.0f:5.0f];[_date setPosition:135.0f: 5.0f];[_year setPosition:220.0f:70.0f];} // January
		if (_numericmonth == 2) {[_months setLocation:200.0f:5.0f];[_date setPosition:105.0f: 5.0f];[_year setPosition:200.0f:70.0f];} // February
		if (_numericmonth == 3) {[_months setLocation:230.0f:5.0f];[_date setPosition:120.0f: 5.0f];[_year setPosition:230.0f:70.0f];} // March
		if (_numericmonth == 4) {[_months setLocation:250.0f:5.0f];[_date setPosition:140.0f: 5.0f];[_year setPosition:250.0f:70.0f];} // April
		if (_numericmonth == 5) {[_months setLocation:270.0f:5.0f];[_date setPosition:160.0f: 5.0f];[_year setPosition:270.0f:70.0f];} // May
		if (_numericmonth == 6) {[_months setLocation:260.0f:5.0f];[_date setPosition:150.0f: 5.0f];[_year setPosition:260.0f:70.0f];} // June
		if (_numericmonth == 7) {[_months setLocation:260.0f:5.0f];[_date setPosition:150.0f: 5.0f];[_year setPosition:260.0f:70.0f];} // July
		if (_numericmonth == 8) {[_months setLocation:230.0f:5.0f];[_date setPosition:120.0f: 5.0f];[_year setPosition:230.0f:70.0f];} // August
		if (_numericmonth == 9) {[_months setLocation:180.0f:5.0f];[_date setPosition:75.0f: 5.0f];[_year setPosition:180.0f:70.0f];} // September
		if (_numericmonth == 10) {[_months setLocation:220.0f:5.0f];[_date setPosition:110.0f: 5.0f];[_year setPosition:220.0f:70.0f];} // October
		if (_numericmonth == 11) {[_months setLocation:200.0f:5.0f];[_date setPosition:105.0f: 5.0f];[_year setPosition:200.0f:70.0f];} // November
		if (_numericmonth == 12) {[_months setLocation:200.0f:5.0f];[_date setPosition:105.0f: 5.0f];[_year setPosition:200.0f:70.0f];} // December
		
		//[_klingonday setPosition:240.0f:5.0f];
		
	} else {
		if (_numericmonth == 1) {[_months setLocation:150.0f:5.0f];[_date setPosition:60.0f: 5.0f];[_year setPosition:150.0f:70.0f];} // January
		if (_numericmonth == 2) {[_months setLocation:140.0f:5.0f];[_date setPosition:50.0f: 5.0f];[_year setPosition:140.0f:70.0f];} // February
		if (_numericmonth == 3) {[_months setLocation:170.0f:5.0f];[_date setPosition:70.0f: 5.0f];[_year setPosition:170.0f:70.0f];} // March 
		if (_numericmonth == 4) {[_months setLocation:200.0f:5.0f];[_date setPosition:80.0f: 5.0f];[_year setPosition:200.0f:70.0f];} // April
		if (_numericmonth == 5) {[_months setLocation:220.0f:5.0f];[_date setPosition:110.0f: 5.0f];[_year setPosition:220.0f:70.0f];} // May
		if (_numericmonth == 6) {[_months setLocation:210.0f:5.0f];[_date setPosition:100.0f: 5.0f];[_year setPosition:210.0f:70.0f];} // June
		if (_numericmonth == 7) {[_months setLocation:210.0f:5.0f];[_date setPosition:100.0f: 5.0f];[_year setPosition:210.0f:70.0f];} // July
		if (_numericmonth == 8) {[_months setLocation:180.0f:5.0f];[_date setPosition:70.0f: 5.0f];[_year setPosition:180.0f:70.0f];} // August
		if (_numericmonth == 9) {[_months setLocation:120.0f:5.0f];[_date setPosition:25.0f: 5.0f];[_year setPosition:120.0f:70.0f];} // September
		if (_numericmonth == 10) {[_months setLocation:150.0f:5.0f];[_date setPosition:50.0f: 5.0f];[_year setPosition:150.0f:70.0f];} // October
		if (_numericmonth == 11) {[_months setLocation:140.0f:5.0f];[_date setPosition:50.0f: 5.0f];[_year setPosition:140.0f:70.0f];} // November
		if (_numericmonth == 12) {[_months setLocation:140.0f:5.0f];[_date setPosition:50.0f: 5.0f];[_year setPosition:140.0f:70.0f];} // December
		
		//[_klingonday setPosition:180.0f:5.0f];
	}
}

- (void)setKlingonmode:(bool)klingonmode
{
	_klingonmode = klingonmode;
	
	if (klingonmode == true) {
		[_date setnewResource:@"DateNumbersKlingon.png"];
		[_year setnewResource:@"DateNumbersKlingon.png"];
	} else {
		[_date setnewResource:@"DateNumbers.png"];
		[_year setnewResource:@"DateNumbers.png"];
	}
	
	[self setNeedsDisplay];
}

- (void)setNumericMonth:(int)numericmonth
{
	_numericmonth = numericmonth;
	[_months setNumericMonth:numericmonth];
	
	NSNumber *nummonth;
	nummonth = [[NSNumber alloc] initWithInt:numericmonth];
	[_klingonmonth setClockstring:[nummonth stringValue]];
	
	[self repositionMonth];
	[self setNeedsDisplay];
}

- (void)setDateString:(NSString *) datestring
{
	[_date setClockstring:datestring];
	
	[self setNeedsDisplay];
}

- (void)setYearString:(NSString *) yearstring
{
	[_year setClockstring:yearstring];
	
	[self setNeedsDisplay];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
	if (_klingonmode == true) {
		[_klingonmonth PaintString];
	} else {
		[_months PaintString];
	}
	
	[_date PaintString];
	[_year PaintString];
}


- (void)dealloc {
    [super dealloc];
}


@end
