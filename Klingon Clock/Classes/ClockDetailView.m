//
//  ClockDetailView.m
//  Klingon Clock
//
//  Created by Danny Draper on 29/12/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import "ClockDetailView.h"


@implementation ClockDetailView

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super initWithCoder:decoder])
    {
		_blandscapemode = false;
		//self.opaque = NO;
        self.backgroundColor = [UIColor clearColor];
		
		//Landscape Clock Numbers
		[_clocknumbers = [[ClockNumbers alloc] init] retain];
		[_clocknumbers Initialise:6.0f:4.0f:155.0f:191.0f:true:45.0f:@"ClockNumbers.png"];
		//[_clocknumbers setClockstring:@"22:55"];
		[_clocknumbers setPosition:1.0f:15.0f];
		
		
		// Portrait Clock Numbers
		[_clocknumberspr = [[ClockNumbers alloc] init] retain];
		[_clocknumberspr Initialise:4.0f:0.0f:136.0f:167.0f:true:39.0f:@"ClockNumbersPR.png"];
		//[_clocknumberspr setClockstring:@"22:57"];
		[_clocknumberspr setPosition:30.0f:15.0f];
		
		
		// Second Numbers
		[_secondnumbers = [[ClockNumbers alloc] init] retain];
		[_secondnumbers Initialise:6.0f:5.0f:42.0f:55.0f:false:0.0f:@"SecondNumbers.png"];
		//[_secondnumbers setClockstring:@"47"];
		[_secondnumbers setPosition:300.0f:200.0f];
		
		
		//[self setKlingonmode:true];
		[self setNeedsDisplay];
    }
    return self;
}

- (void)setLandscapemode:(bool)landscape
{
	_blandscapemode = landscape;
	
	if (landscape == true) {
		
	} else {
	
	}
}

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
		//self.opaque = NO;
		//self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)setClockstring:(NSString *)clockstring
{
	[_clocknumbers setClockstring:clockstring];
	[_clocknumberspr setClockstring:clockstring];
}

- (void)setSecondstring:(NSString *)secondstring
{
	[_secondnumbers setClockstring:secondstring];
}

- (void)setKlingonmode:(bool)klingonmode
{
	if (klingonmode == true) {
		[_clocknumbers setnewResource:@"ClockNumbersKlingon.png"];
		[_clocknumberspr setnewResource:@"ClockNumbersKlingonPR.png"];
		[_secondnumbers setnewResource:@"SecondNumbersKlingon.png"];
	} else {
		[_clocknumbers setnewResource:@"ClockNumbers.png"];
		[_clocknumberspr setnewResource:@"ClockNumbersPR.png"];
		[_secondnumbers setnewResource:@"SecondNumbers.png"];	
	}
}

- (void)refreshDisplay
{
	[self setNeedsDisplay];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
	if (_blandscapemode == true) {
		[_clocknumbers PaintString];
	} else {
		[_clocknumberspr PaintString];
	}
	
	
	[_secondnumbers PaintString];
}


- (void)dealloc {
    [super dealloc];
}


@end
